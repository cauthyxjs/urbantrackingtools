#Minimum version of CMAKE used
cmake_minimum_required(VERSION 2.8)
set(LIBOUTPUT bin/${CMAKE_BUILD_TYPE})


file(
    GLOB_RECURSE
	source_files
	src/*
	include/*
	include/dp/*
include/jmo/*
include/lb/*
include/tb/*
)

include_directories(include include/dp include/jmo include/lb include/tb)

add_library(BGSLib STATIC ${source_files})
target_link_libraries(BGSLib ${OpenCV_LIBS}) 
