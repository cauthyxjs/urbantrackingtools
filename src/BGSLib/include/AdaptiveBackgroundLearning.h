#pragma once

#include <iostream>
#include <opencv2/opencv.hpp>
#include <highgui.h>

#include "IBGS.h"

class AdaptiveBackgroundLearning : public IBGS
{
	
private:
  bool firstTime;
  cv::Mat img_background;
  double alpha;
  long limit;
  long counter;
  double minVal;
  double maxVal;
  bool enableThreshold;
  int threshold;
  bool showForeground;
  bool showBackground;

public:
  AdaptiveBackgroundLearning();
  ~AdaptiveBackgroundLearning();
  virtual int getNbChannel() { return 1; }; 
  //static IBGS* getInstance() { return new AdaptiveBackgroundLearning();}
  void process(const cv::Mat &img_input, cv::Mat &img_output);

private:
  void saveConfig();
  void loadConfig();
};

