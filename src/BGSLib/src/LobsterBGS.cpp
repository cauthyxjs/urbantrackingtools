#include "LobsterBGS.h"

#include "BackgroundSubtractorLOBSTER.h"
#include <vector>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <opencv2/features2d/features2d.hpp>
#include <sstream>

LobsterBGS::LobsterBGS()
: mBGSSubstractor(nullptr)
{

}


LobsterBGS::~LobsterBGS()
{
	delete mBGSSubstractor;
}

void LobsterBGS::init(const cv::Mat& ref)
{
	if(mBGSSubstractor)
		delete mBGSSubstractor;

	mBGSSubstractor = new BackgroundSubtractorLOBSTER();
	std::vector<cv::KeyPoint> voKeyPoints;
	mBGSSubstractor->initialize(ref, voKeyPoints);
	
}

//Gray input
void LobsterBGS::process(const cv::Mat &img_input, cv::Mat &img_output)
{
	if(!mBGSSubstractor)
	{
		init(img_input);
		img_output = cv::Mat::zeros(img_input.rows, img_input.cols,CV_MAKETYPE(IPL_DEPTH_8U,1));
	}
	else
		(*mBGSSubstractor)(img_input, img_output);
}



void LobsterBGS::removeBlobFromModel(const cv::Mat& labelFrame, const std::set<int> labelSet, const std::vector<cv::Rect>& rectList, const cv::Mat& currentImageHistory)
{
	(*mBGSSubstractor).removeBlobFromModel(labelFrame, labelSet, rectList, currentImageHistory);
}


void LobsterBGS::saveConfig()
{

}

void LobsterBGS::loadConfig()
{
	//TODO: Instantiate bgs substractor from here instead of constructor to load parameters
}
