#include "GPUExtendedGaussianMixtureBGS.h"

#ifdef CUDA
GPUExtendedGaussianMixtureBGS::GPUExtendedGaussianMixtureBGS()
: pGMMParams(NULL)
, pGMM(NULL)
{

}

GPUExtendedGaussianMixtureBGS::~GPUExtendedGaussianMixtureBGS()
{
	if(pGMM)
		cvReleaseFastBgGMM(&pGMM);	
	delete pGMMParams;
}


void GPUExtendedGaussianMixtureBGS::process(const cv::Mat &img_input, cv::Mat &img_output)
{
	IplImage im = img_input;
	if(pGMMParams == NULL) //We need to init
	{
		pGMMParams = cvCreateFastBgGMMParams(img_input.cols, img_input.rows);
		pGMMParams->bShadowDetection = false;
		pGMMParams->fAlphaT = 0.008f; //We will need to take all param from config
	
		
		pGMM = cvCreateFastBgGMM(pGMMParams, &im);
	}

	cvUpdateFastBgGMM(pGMM, &im);
	cv::Mat bgsWithShadow = pGMM->h_outputImg;
	cv::threshold(bgsWithShadow, img_output, 200, 255, cv::THRESH_BINARY);
}

void GPUExtendedGaussianMixtureBGS::saveConfig()
{

}

void GPUExtendedGaussianMixtureBGS::loadConfig()
{

}
#endif