#include "AppKeyPressHandler.h"
#include <QKeyEvent>
#include "QtGroundTruthBuilderApp.h"

AppKeyPressHandler::AppKeyPressHandler(std::set<Qt::Key> keysToCatch, QtGroundTruthBuilderApp* eventReceiver)
: mKeysToCatch(keysToCatch)
, mEventReceiver(eventReceiver)
{

}

bool AppKeyPressHandler::eventFilter(QObject *obj, QEvent *event)
{
	bool handled = false;
	 if (event->type() == QEvent::KeyPress) 
	 {
         QKeyEvent* keyEvent = static_cast<QKeyEvent *>(event);
		 if(mKeysToCatch.find((Qt::Key)keyEvent->key()) != mKeysToCatch.end())
		 {
			 
			 mEventReceiver->keyPressEvent(keyEvent);
			 handled = true;
		 }         
     } 
	 return handled ? true : QObject::eventFilter(obj, event);



}