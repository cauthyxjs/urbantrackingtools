#include "ExportVideoTask.h"
#include "AppContext.h"
#include "InputFrameProviderIface.h"



#include "DrawableSceneView.h"
#include "ObjectObservation.h"
#include "InputFrameProviderIface.h"
#include "SceneObject.h"
#include <map>

#include <opencv2/opencv.hpp>
#include "ExportVideoTask.h"

 ExportVideoTask::ExportVideoTask(StatusProgressBar* progressBarControl, QString filePath, bool displayTrail, bool displayBoundingBox, int startTime, int endTime, float fps, InputFrameProviderIface* vfm, AppContext* appContext)
: BaseTask(progressBarControl, "Annotated video exportation")
, mFilePath(filePath)
, mDisplayTrail(displayTrail)
, mDisplayBoundingBox(displayBoundingBox)
, mStartFrame(startTime)
, mEndFrame(endTime)
, mFPS(fps)
, m_np_Vfm(vfm)
, mContext(appContext)
{
}

void ExportVideoTask::run()
{
	std::map<SceneObject*, std::vector<cv::Point2f>> sceneObjectTrail;

	cv::Mat tmpFrame;
	if(m_np_Vfm->isOpen() && m_np_Vfm->seekAtFramePos(0) && m_np_Vfm->getNextFrame(tmpFrame))
	{		
		int codecType = CV_FOURCC('I','4','2','0');
		#ifdef _WIN32
			codecType = -1; //Option windows specific qui donne le choix � l'utilisateur
		#endif

		cv::VideoWriter videoWriter(mFilePath.toStdString(),codecType, mFPS, cv::Size(tmpFrame.cols, tmpFrame.rows)); 
		if(videoWriter.isOpened())
		{

			auto& timestampMap = mContext->getTimestampToScene();
			int numberFrame = mEndFrame-mStartFrame+1;
			for(int t = mStartFrame; t <= mEndFrame; ++t)
			{
				updateProgressBarPercent(100*((float)(t-mStartFrame)/(float)numberFrame));
				
				m_np_Vfm->seekAtFramePos(t);
				if(m_np_Vfm->getNextFrame(tmpFrame))
				{
					auto itSceneViewPair = timestampMap.find(t);
					if(itSceneViewPair != timestampMap.end())
					{
						DrawableSceneView* dsv = itSceneViewPair->second;
						const QList<QGraphicsItem*>& items = dsv->items();
						for (auto itemIt = items.begin(); itemIt != items.end(); ++itemIt)
						{
							ObjectObservation* obj = dynamic_cast<ObjectObservation*>(*itemIt);
							if(obj)
							{
								SceneObject* so = obj->getSceneObject();
								const Color& Qtcolor = so->getColor();
								cv::Scalar cvColor = cv::Scalar(Qtcolor.B, Qtcolor.G, Qtcolor.R);

								const QRectF& rect = obj->rect();
								cv::Point2f centroid(rect.center().x(), rect.center().y());
								if(mDisplayTrail)
								{
									std::vector<cv::Point2f>& centroidList = sceneObjectTrail[so];
									centroidList.push_back(centroid);
									for(unsigned int p = 0; p < centroidList.size()-1; ++p)
									{
										cv::line(tmpFrame, centroidList[p], centroidList[p+1], cvColor, 3);
									}
									cv::circle(tmpFrame, centroidList.back(), 3, cvColor, 3);						
								}

								if(mDisplayBoundingBox)
								{
									cv::Rect bb(rect.x(), rect.y(), rect.width(), rect.height());
									cv::rectangle(tmpFrame, bb, cvColor, 3);
									cv::rectangle(tmpFrame, cv::Rect(bb.x - 5, bb.y - 7, 12*so->getId().size(), 14), cv::Scalar(0,0,0), CV_FILLED);
									cv::putText(tmpFrame, so->getId(), cv::Point(bb.x-5, bb.y+4), cv::FONT_HERSHEY_DUPLEX, 0.5, cv::Scalar(255,255,255),1,8, false);
									//Number
								}
							}
						}
					}
					videoWriter << tmpFrame;
				}
			}
			LOGINFO("Video saved");
		}
		else
		{
			LOGERROR("Can't save video file");
		}

		videoWriter.release();
	}
}
