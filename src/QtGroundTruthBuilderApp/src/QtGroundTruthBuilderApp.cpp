#include "QtGroundTruthBuilderApp.h"
#include <QInputDialog>
#include <QString>
#include <QDir>
#include <QFileDialog>
#include "InputVideoFileModule.h"
#include <QGraphicsScene>
#include "DrawableSceneView.h"
#include <QBrush>
#include "SceneObject.h"
#include <QRadioButton>
#include <QSpacerItem>
#include "Pets2001XmlImporter.h"
#include "Pets2009XmlImporter.h"
#include <QVariant>

#include "CAVIARImporter.h"
#include "UrbanTrackerImporter.h"
#include "ObjectObservation.h"
#include "AppContext.h"
#include "TrafficIntelligenceGTImporter.h"
#include "TrafficIntelligenceSQLImporter.h"
#include "BlobDetector.h"
#include "LundImporter.h"
#include "StringHelpers.h"
#include <QMouseEvent>
#include "AppKeyPressHandler.h"
#include <QStatusBar>
#include "NegativeSampleDlg.h"
#include <QMessageBox>
#include <qtimer.h>
#include "InputFrameListModule.h"
#include "MergeObjectDlg.h"
#include "ChangeIdDlg.h"
#include "DeleteRangeDlg.h"
#include "ApplyHomographyDlg.h"
#include "InterpolationManager.h"
#include "ApplyMaskDlg.h"
#include "ExportVideoDlg.h"
#include "qprogressbar.h"
#include "qthread.h"
#include "StatusProgressBar.h"
#include "ProgressDialog.h"
#include <qmessagebox.h>
#include "OpenCVHelpers.h"
#include "PolyTrackImporter.h"
#include "MatrixIO.h"
#include "ExporterTask.h"

#include "PolyTrackExporter.h"
#include "UrbanTrackerExporter.h"



void QtGroundTruthBuilderApp::SaveDatabase()
{
	QObject* sender = QObject::sender();
	QAction* exporterAction = (QAction*) sender;

	ExporterTask* exporter = mActionToExporter[exporterAction];
	
	QString filePath = QFileDialog::getSaveFileName(this, "Save " +exporter->getName() +  " database",	"", exporter->getExtension());	
	exporter->setSaveInfo(filePath.toStdString(), mCurrentGraphicsScene->sceneRect().bottom(), mCurrentGraphicsScene->sceneRect().right());
	runAsyncTask(exporter);
}


void QtGroundTruthBuilderApp::SaveCurrentFrame()
{
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save Image"),"",tr("PNG (*.png)"));
	if(fileName != "")
		cv::imwrite(fileName.toStdString(),	mCurrentFrameMat);
}

void QtGroundTruthBuilderApp::SaveCurrentFrameAnnotation()
{
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save Annotated Image"),"",tr("PNG (*.png)"));
	if(fileName != "")
	{
		cv::Mat tmpFrame = mCurrentFrameMat.clone();
		const QList<QGraphicsItem*>& items = mCurrentGraphicsScene->items();
		for (auto itemIt = items.begin(); itemIt != items.end(); ++itemIt)
		{
			ObjectObservation* obj = dynamic_cast<ObjectObservation*>(*itemIt);
			if(obj)
			{
				SceneObject* so = obj->getSceneObject();
				const Color& Qtcolor = so->getColor();
				cv::Scalar cvColor = cv::Scalar(Qtcolor.B, Qtcolor.G, Qtcolor.R);

				const QRectF& rect = obj->rect();
				cv::Point2f centroid(rect.center().x(), rect.center().y());

				cv::Rect bb(rect.x(), rect.y(), rect.width(), rect.height());
				cv::rectangle(tmpFrame, bb, cvColor, 3);
				cv::rectangle(tmpFrame, cv::Rect(bb.x - 5, bb.y - 7, 12*so->getId().size(), 14), cv::Scalar(0,0,0), CV_FILLED);
				cv::putText(tmpFrame, so->getId(), cv::Point(bb.x-5, bb.y+4), cv::FONT_HERSHEY_DUPLEX, 0.5, cv::Scalar(255,255,255),1,8, false);
			}
		}
		cv::imwrite(fileName.toStdString(),	tmpFrame);
	}

}

void QtGroundTruthBuilderApp::OpenChangeIdDlg()
{
	ChangeIdDlg dlg(this, mAppContext);

	if( dlg.exec() == QDialog::Accepted)	
		dlg.run();
	
}

void QtGroundTruthBuilderApp::OpenMaskDlg()
{
	ApplyMaskDlg dlg(this, mAppContext);

	if( dlg.exec() == QDialog::Accepted)	
		dlg.run();
}


void QtGroundTruthBuilderApp::OpenMergeObjectDlg()
{
	MergeObjectDlg dlg(this, mAppContext);
	if( dlg.exec() == QDialog::Accepted )
		dlg.run();
	UpdateObjectIdList();
}

void QtGroundTruthBuilderApp::OpenHomographyDlg()
{
	ApplyHomographyDlg dlg(this, mAppContext);
	if( dlg.exec() == QDialog::Accepted )
		dlg.run();
}


void QtGroundTruthBuilderApp::OpenDeleteRangeDlg()
{

	DeleteRangeDlg dlg(this, mProgressBar, mAppContext);
	if( dlg.exec() == QDialog::Accepted )
		runAsyncTask(&dlg);
}

void QtGroundTruthBuilderApp::OpenSaveAnnotatedVideoDlg()
{
	ExportVideoDlg dlg(this, mProgressBar, mAppContext, mVfm);
	if( dlg.exec() == QDialog::Accepted)	
		runAsyncTask(&dlg);
	
}

void QtGroundTruthBuilderApp::runAsyncTask(ProgressDialog* dlg)
{
	runAsyncTask(dlg->getBaseTask());
}

void QtGroundTruthBuilderApp::finishImport()
{
	UpdateObjectTypeList();
	seekToFrame(0);
	OnNewObjectAdded();
	const auto& sceneObjectList = mAppContext->getSceneObjectList();
	unsigned int maxId = 0;
	for(auto it = sceneObjectList.begin(); it != sceneObjectList.end(); ++it)
	{
		unsigned int currentId;
		if(Utils::String::StringToUInt((*it)->getId(), currentId))
		if(currentId > maxId)
			maxId = currentId;
	}
	mAppContext->setLastIdOfSet(maxId);
	finishTask();
}

void QtGroundTruthBuilderApp::runAsyncImport(GTImporter* task)
{
	if(!mCurrentTask && !mCurrentTaskThread)
	{
		mCurrentTask = task;
		
		mCurrentTaskThread = new QThread(this);
		//mCurrentTask->moveToThread(mCurrentTaskThread); //Creating in an outside thread make the objets observation input fail
		connect(mCurrentTaskThread, SIGNAL(started()), mCurrentTask, SLOT(process()));
		connect(mCurrentTask, SIGNAL(finished()), mCurrentTaskThread, SLOT(quit()));
		connect(mCurrentTaskThread, SIGNAL(finished()), this, SLOT(finishImport()));
		mCurrentTaskThread->start();
	}
	else
	{
		QMessageBox::critical(this, "Can't run task yet", "You need to finish the previous task before starting a new task.");
	}
}

void QtGroundTruthBuilderApp::runAsyncTask(BaseTask* task)
{
	if(!mCurrentTask && !mCurrentTaskThread)
	{
		mCurrentTask = task;
		mCurrentTaskThread = new QThread(this);
		
		bool connected = connect(mCurrentTaskThread, SIGNAL(started()), mCurrentTask, SLOT(process()));
		connect(mCurrentTask, SIGNAL(finished()), mCurrentTaskThread, SLOT(quit()));
		connect(mCurrentTaskThread, SIGNAL(finished()), this, SLOT(finishTask()));
		//connect(mCurrentTaskThread, SIGNAL(started()), qApp, SLOT(aboutQt()));
		LOGASSERT(connected, "Signal was not connected to finishTask");
		//mCurrentTask->moveToThread(mCurrentTaskThread);
		mCurrentTaskThread->start();
	}
	else
	{
		QMessageBox::critical(this, "Can't run task yet", "You need to finish the previous task before starting a new task.");
	}
}


void QtGroundTruthBuilderApp::DisplayMaxNbBoxes()
{
	const std::map<unsigned int, DrawableSceneView*>& timestampToScene = mAppContext->getTimestampToScene();

	int maxNbBoxes = 0;
	int totalBoxes = 0;
	for(auto sceneIt = timestampToScene.begin(); sceneIt != timestampToScene.end(); ++sceneIt)
	{		
		DrawableSceneView* dsw = sceneIt->second;
		int tmpNbBoxes = dsw->items().size();
		if(tmpNbBoxes > maxNbBoxes)
			maxNbBoxes = tmpNbBoxes;		
		totalBoxes+=tmpNbBoxes;		
	}

	QMessageBox::information(this, "Video stats", QString::number(maxNbBoxes) + "simultaneous boxes.\n" + QString::number(totalBoxes) + " boxes in total.");
}

void QtGroundTruthBuilderApp::finishTask()
{
	std::cout << "Finish task called !\n";
	bool disconnected = disconnect(mCurrentTaskThread, SIGNAL(started()), mCurrentTask, SLOT(process()));
	disconnect(mCurrentTask, SIGNAL(finished()), mCurrentTaskThread, SLOT(quit()));
	disconnect(mCurrentTaskThread, SIGNAL(finished()), this, SLOT(finishTask()));
	disconnect(mCurrentTaskThread);
	if(mCurrentTask->isOwnedByThread()) 
	{
		std::cout << "Deleted task\n";
		delete mCurrentTask;
		mCurrentTask = nullptr;	
		
	}
	else 
		mCurrentTask = nullptr;

	//LOGASSERT(disconnected, "Signal were not disconnected correctly");
	delete mCurrentTaskThread;
	mCurrentTaskThread = nullptr;
}


QtGroundTruthBuilderApp::QtGroundTruthBuilderApp(QWidget *parent, Qt::WindowFlags flags)
: QMainWindow(parent, flags)
, mVfm(nullptr)
, mInitOnFirstFrame(true)
, mCurrentGraphicsScene(nullptr)
, mAppContext(new AppContext(this))
, mPlayTimer(new QTimer(this))
, mProgressBar(nullptr)
, mCurrentTask(nullptr)
, mCurrentTaskThread(nullptr)
, mCurrentGraphicsViewScaleFactor(1.0f)
{
	mUi.setupUi(this);
	mProgressBar = new StatusProgressBar(mUi.statusBar);

	bool connected = connect(mUi.actionLoadVideo, SIGNAL(triggered()), this, SLOT(LoadVideo()));

	connect(mUi.actionAddObjectType, SIGNAL(triggered()), this, SLOT(AddNewObjectType()));
	connect(mUi.actionMerge_objects, SIGNAL(triggered()), this, SLOT(OpenMergeObjectDlg()));
	connect(mUi.actionChangeId, SIGNAL(triggered()), this, SLOT(OpenChangeIdDlg()));	
	connect(mUi.actionDeleteRange, SIGNAL(triggered()), this, SLOT(OpenDeleteRangeDlg()));	
	connect(mUi.actionApply_homography, SIGNAL(triggered()), this, SLOT(OpenHomographyDlg()));	
	connect(mUi.actionApplyMask, SIGNAL(triggered()), this, SLOT(OpenMaskDlg()));	
	connect(mUi.actionExport_video, SIGNAL(triggered()), this, SLOT(OpenSaveAnnotatedVideoDlg()));
	connect(mUi.interpolateBtn, SIGNAL(pressed()), this, SLOT(OnInterpolateBtnTriggered()));	

	connect(mUi.GoNbFrameAfter, SIGNAL(pressed()), this, SLOT(GoToNextFrame()));
	connect(mUi.GoNbFrameBefore, SIGNAL(pressed()), this, SLOT(GoToPreviousFrame()));	
	connect(mUi.objectDescriptionEdit, SIGNAL(editingFinished ()), this, SLOT(DescriptionFinish()));
	connect(mUi.ObjectTypeList, SIGNAL(currentIndexChanged(int)), this, SLOT(OnObjectTypeSelection(int)));
	connect(mUi.ObjectIdList, SIGNAL(currentIndexChanged(int)), this, SLOT(OnObjectIdSelection(int)));
	
	
	connect(mUi.actionSave_current_frame, SIGNAL(triggered()), this, SLOT(SaveCurrentFrame()));
	connect(mUi.actionSave_current_frame_annotation, SIGNAL(triggered()), this, SLOT(SaveCurrentFrameAnnotation()));
	connect(mUi.actionMax_number_of_boxes, SIGNAL(triggered()), this, SLOT(DisplayMaxNbBoxes()));


	

	connect(mUi.CurrentFrameObjetListWidget, SIGNAL(itemSelectionChanged()), this, SLOT(onListItemSelectionChange()));
	connect(mUi.actionExtractObjects, SIGNAL(triggered()), this, SLOT(ExtractSubImage()));
	connect(mUi.actionExtract_negative_objects, SIGNAL(triggered()), this, SLOT(OpenNegativeSampleDlg()));
	connect(mUi.actionCount_number_of_box, SIGNAL(triggered()), this, SLOT(countNumberOfBox()));
	connect(this, SIGNAL(OnFrameChange()), this, SLOT(UpdateCurrentFrameObjectList()));
	connect(mUi.actionAbout, SIGNAL(triggered()), this, SLOT(displayAboutDlg()));
	connect(mUi.playVideoBtn, SIGNAL(pressed()), this, SLOT(onPlayPause()));
	connect(mPlayTimer, SIGNAL(timeout()), this, SLOT(onPlayPauseNewFrame()));




	mUi.CurrentFrameWidget->setAlignment(Qt::AlignLeft | Qt::AlignTop);
	mUi.CurrentFrameWidget->setRenderHints( QPainter::Antialiasing |
		QPainter::TextAntialiasing |
		QPainter::SmoothPixmapTransform |
		QPainter::HighQualityAntialiasing );
	mUi.CurrentFrameWidget->setCacheMode(QGraphicsView::CacheBackground);
	UpdateObjectIdList();
	
	SceneObjectSelectionChanged();
	UpdateCurrentFrameObjectList();
	UpdateObjectTypeList();
	std::set<Qt::Key> catchedKeys; catchedKeys.insert(Qt::Key_Left); catchedKeys.insert(Qt::Key_Right);
	AppKeyPressHandler* keyHandler = new AppKeyPressHandler(catchedKeys, this);
	QCoreApplication::instance()->installEventFilter(keyHandler);
}

void QtGroundTruthBuilderApp::OnInterpolateBtnTriggered()
{
	int startFrame = mUi.interpolationStart->value();
	int endFrame = mUi.interpolationEnd->value();
	const auto& list = mCurrentGraphicsScene->selectedItems();
	if(!list.empty())
	{
		for(auto it = list.begin(); it != list.end(); ++it)
		{
			ObjectObservation* obj = dynamic_cast<ObjectObservation*>(*it);
			if(obj && obj->getSceneObject())
				Utils::interpolate(mAppContext, obj->getSceneObject(), startFrame, endFrame, mUi.interpolateAllCb->isChecked());
		}

	}
}
void QtGroundTruthBuilderApp::onPlayPauseNewFrame()
{
	GoToNextFrame();
}

void QtGroundTruthBuilderApp::OnObjectIdSelection(int idx)
{
	if(idx != -1)
	{
		if(mCurrentGraphicsScene)
		{
			auto list = mCurrentGraphicsScene->selectedItems();

			QVariant v = mUi.ObjectIdList->itemData(idx, Qt::UserRole);
			SceneObject* so = (SceneObject*) v.value<void*>();

			if(!list.empty() && so)
			{
				ObjectObservation* obs = dynamic_cast<ObjectObservation*>(list[0]);
				obs->associateToSceneObject(so);
			}
		}
	}
}


void QtGroundTruthBuilderApp::OnObjectTypeSelection(int idx)
{
	if(idx != -1)
	{
		QVariant v = mUi.ObjectTypeList->itemData(idx, Qt::UserRole);
		QString objectType;

		int type = v.toInt();
		if(type == NEWTYPE)
		{
			objectType = AddNewObjectType();
		}
		else if(type == EXISTINGTYPE)
		{
			objectType = mUi.ObjectTypeList->itemText(idx);
		}
		else
		{		
			assert(false); //Unknown type
		}

		if(objectType != "")
		{
			auto list = mCurrentGraphicsScene->selectedItems();
			if(!list.empty())
			{
				for(auto it = list.begin(); it != list.end() ;++it)
				{
					ObjectObservation* obj = dynamic_cast<ObjectObservation*>(*it);
					SceneObject* sceneObj = obj->getSceneObject();
					if(sceneObj)
						sceneObj->setType(objectType.toStdString());
				}
			}
		}
	}

}

void QtGroundTruthBuilderApp::UpdateObjectTypeList()
{
	disconnect(mUi.ObjectTypeList, SIGNAL(currentIndexChanged(int)), this, SLOT(OnObjectTypeSelection(int)));
	mUi.ObjectTypeList->clear();

	auto objectTypeList = mAppContext->getObjectTypeManager().getTypeList();
	for(auto it = objectTypeList.begin(); it != objectTypeList.end(); ++it)
	{
		QVariant v;
		v.setValue((int)EXISTINGTYPE);
		QString typeName = QString((*it).second.c_str());
		mUi.ObjectTypeList->addItem(typeName, v);
	}
	QVariant v;
	v.setValue((int)NEWTYPE);
	mUi.ObjectTypeList->addItem("New object type", v);
	connect(mUi.ObjectTypeList, SIGNAL(currentIndexChanged(int)), this, SLOT(OnObjectTypeSelection(int)));
	
}





void QtGroundTruthBuilderApp::onPlayPause()
{
	if(mAppContext->getVFM())
	{
		if(mPlayTimer->isActive())
		{
			mPlayTimer->stop();
			mUi.playVideoBtn->setText("Play");
		}
		else
		{
			mUi.playVideoBtn->setText("Pause");
			mPlayTimer->setInterval(1./mVfm->getNbFPS());
			mPlayTimer->start();
		}
	}

}

void QtGroundTruthBuilderApp::displayAboutDlg()
{
	QMessageBox::about(this, "About", "This application was created during the master of Jean-Philippe Jodoin \
at �cole Polytechnique de Montr�al under the supervision of Guillaume-Alexandre Bilodeau \
and Nicolas Saunier. This version is incomplete, please do not distribute without autorization. \
You can write to jpjodoin@gmail.com to get proper autorization. This will insure proper distribution \
of the latest releases.");
}


void QtGroundTruthBuilderApp::countNumberOfBox()
{
	unsigned int nbItem = 0;
	const std::map<unsigned int, DrawableSceneView*>& timestampToScene = mAppContext->getTimestampToScene();

	for(auto sceneIt = timestampToScene.begin(); sceneIt != timestampToScene.end(); ++sceneIt)
	{		
		DrawableSceneView* dsw = sceneIt->second;
		nbItem+=dsw->items().size();		
	}
	QMainWindow::statusBar()->showMessage("There are " + QString::number(nbItem) + " box.");
}



void QtGroundTruthBuilderApp::onListItemSelectionChange()
{
	

	const auto& selectedItems = mUi.CurrentFrameObjetListWidget->selectedItems();
	if(!selectedItems.empty())
	{
		QListWidgetItem* item = selectedItems[0];
		QVariant v = item->data(Qt::UserRole);
		ObjectObservation* objObs = (ObjectObservation *) v.value<void *>();
		if(objObs)
		{
			objObs->scene()->clearSelection();
			objObs->setSelected(true);
		}
	}
}


void QtGroundTruthBuilderApp::OnNewObjectAdded()
{
	UpdateObjectIdList();
}


void QtGroundTruthBuilderApp::keyPressEvent(QKeyEvent *event)
{
	if(event->key() == Qt::Key_Left) 
	{
		GoToPreviousFrame();
	}
	else if(event->key() == Qt::Key_Right) 
	{
		GoToNextFrame();
	}
	else if(event->key() == Qt::Key_S)
	{
		onPlayPause();
	}
	else 
		QWidget::keyPressEvent(event);
}

void QtGroundTruthBuilderApp::UpdateObjectIdList()
{
	disconnect(mUi.ObjectIdList, SIGNAL(currentIndexChanged(int)), this, SLOT(OnObjectIdSelection(int)));
	int currentItemCount = mUi.ObjectIdList->count();
	int currentIdx = mUi.ObjectIdList->currentIndex();
	QString currentText = mUi.ObjectIdList->itemText(currentIdx);
	mUi.ObjectIdList->clear();
	const std::vector<SceneObject*>& sceneObjectList = mAppContext->getSceneObjectList();
	for(auto it = sceneObjectList.begin(); it != sceneObjectList.end() ;++it)
	{
		SceneObject* so = *it;
		std::string name = so->getId() + " " + so->getDescription() + " " + so->getType();
		QVariant v;
		v.setValue((void*)so);
		mUi.ObjectIdList->addItem(QString(name.c_str()), v);	
	}
	QVariant v;
	v.setValue((void*)nullptr);
	mUi.ObjectIdList->addItem("New object", v);
	connect(mUi.ObjectIdList, SIGNAL(currentIndexChanged(int)), this, SLOT(OnObjectIdSelection(int)));

	//We put back the same index unless an object was added, then we put the new object index
	if(mUi.ObjectIdList->count() > currentItemCount)
	{
		mUi.ObjectIdList->setCurrentIndex(mUi.ObjectIdList->count() - 2);
	}
	else
	{
		int idx = mUi.ObjectIdList->findText(currentText);
		mUi.ObjectIdList->setCurrentIndex(idx);
	}

}

void QtGroundTruthBuilderApp::DescriptionFinish()
{
	auto list = mCurrentGraphicsScene->selectedItems();
	if(!list.empty())
	{
		for(auto it = list.begin(); it != list.end() ;++it)
		{
			ObjectObservation* obj = dynamic_cast<ObjectObservation*>(*it);
			SceneObject* sceneObj = obj->getSceneObject();
			if(sceneObj)
				sceneObj->setDescription(mUi.objectDescriptionEdit->text().toStdString());
			else
			{				
				sceneObj = mAppContext->getNewObject();
				mAppContext->getSceneObjectList().push_back(sceneObj);			
				obj->associateToSceneObject(sceneObj);
			}
		}		
	}
}


void QtGroundTruthBuilderApp::registerExporters()
{
	for (auto it = mActionToExporter.begin(); it != mActionToExporter.end(); ++it)
	{
		disconnect((*it).first);
		delete (*it).second;
	}

	mActionToExporter.clear();
	mNameToExporter.clear();
	std::vector<ExporterTask*> exporterList;
	mUi.menuSave_Ground_Truth->clear(); //We delete all the children exporter from the menu
	if(mCurrentGraphicsScene)
	{
		exporterList.push_back(new PolyTrackExporter(mProgressBar, mAppContext));
		exporterList.push_back(new UrbanTrackerExporter(mProgressBar, mAppContext));
		

		
		bool connected = true;
		for(unsigned int i = 0; i < exporterList.size(); ++i)
		{
			QAction* exporterAction = new QAction("Save "+exporterList[i]->getName(), mUi.menuSave_Ground_Truth);
			mUi.menuSave_Ground_Truth->addAction(exporterAction);
			connect(exporterAction, SIGNAL(triggered()), this, SLOT(SaveDatabase()));
			mActionToExporter.insert(std::pair<QAction*, ExporterTask*>(exporterAction, exporterList[i]));	
			mNameToExporter.insert(std::make_pair(exporterList[i]->getName(), exporterList[i]));
		}
	}
}


void QtGroundTruthBuilderApp::registerImporters()
{
	for (auto it = mActionToImporter.begin(); it != mActionToImporter.end(); ++it)
	{
		disconnect((*it).first);
		delete (*it).second;
	}

	mActionToImporter.clear();
	mNameToImporter.clear();
	std::vector<GTImporter*> importer;
	mUi.menuLoad_Ground_Truth->clear(); //We delete all the children importers from the menu
	if(mCurrentGraphicsScene)
	{
		importer.push_back(new PolyTrackImporter(mProgressBar, mAppContext, mCurrentGraphicsScene->sceneRect().bottom(), mCurrentGraphicsScene->sceneRect().right()));
		importer.push_back(new UrbanTrackerImporter(mProgressBar, mAppContext, mCurrentGraphicsScene->sceneRect().bottom(), mCurrentGraphicsScene->sceneRect().right()));
		importer.push_back(new Pets2001XmlImporter(mProgressBar, mAppContext, mCurrentGraphicsScene->sceneRect().bottom(), mCurrentGraphicsScene->sceneRect().right()));
		importer.push_back(new Pets2009XmlImporter(mProgressBar, mAppContext, mCurrentGraphicsScene->sceneRect().bottom(), mCurrentGraphicsScene->sceneRect().right()));
		importer.push_back(new LundImporter(mProgressBar, mAppContext, mCurrentGraphicsScene->sceneRect().bottom(), mCurrentGraphicsScene->sceneRect().right()));
		importer.push_back(new TrafficIntelligenceGTImporter(mProgressBar, mAppContext, mCurrentGraphicsScene->sceneRect().bottom(), mCurrentGraphicsScene->sceneRect().right()));
		importer.push_back(new CaviarImporter(mProgressBar, mAppContext, mCurrentGraphicsScene->sceneRect().bottom(), mCurrentGraphicsScene->sceneRect().right()));
		importer.push_back(new TrafficIntelligenceSQLImporter(mProgressBar, mAppContext, mCurrentGraphicsScene->sceneRect().bottom(), mCurrentGraphicsScene->sceneRect().right()));
		
		
		bool connected = true;
		for(unsigned int i = 0; i < importer.size(); ++i)
		{
			QAction* importerAction = new QAction("Load "+importer[i]->getName(), mUi.menuLoad_Ground_Truth);
			mUi.menuLoad_Ground_Truth->addAction(importerAction);
			connect(importerAction, SIGNAL(triggered()), this, SLOT(LoadDatabase()));
			mActionToImporter.insert(std::pair<QAction*, GTImporter*>(importerAction, importer[i]));	
			mNameToImporter.insert(std::make_pair(importer[i]->getShortName(),importer[i]));
		}
	}
}

QtGroundTruthBuilderApp::~QtGroundTruthBuilderApp()
{
	delete mVfm;
	delete mAppContext;
	for (auto it = mActionToImporter.begin(); it != mActionToImporter.end(); ++it)
	{
		disconnect((*it).first);
		delete (*it).second;
	}

	for (auto it = mActionToExporter.begin(); it != mActionToExporter.end(); ++it)
	{
		disconnect((*it).first);
		delete (*it).second;
	}
	//delete mBlobDetector;
}

void QtGroundTruthBuilderApp::LoadVideo()
{
	QString fileName = QFileDialog::getOpenFileName(this,
		tr("Load Video File"), "",
		tr("All Files (*)"));
	if (fileName.isEmpty())
		return;
	else
		LoadVideo(fileName);
}

void QtGroundTruthBuilderApp::LoadVideo(QString fileName)
{
	delete mVfm;
	std::string name = fileName.toStdString();
	if(fileName.endsWith(".txt"))
	{
		int posLastChar = std::max((int)name.find_last_of('\\'), (int)name.find_last_of('/'));
		std::string basePath = name.substr(0, posLastChar);
		mVfm = new InputFrameListModule(basePath, name);
	}
	else
		mVfm = new InputVideoFileModule(name);
	
	mAppContext->setInputFileModule(mVfm);
	mInitOnFirstFrame = true;
	seekToFrame(0);		
	registerImporters();	
	registerExporters();
}

void QtGroundTruthBuilderApp::LoadDatabase(const QString& type, const QString& path)
{
	auto nameIt = mNameToImporter.find(type);
	if(nameIt != mNameToImporter.end())
	{
		GTImporter* importer = (*nameIt).second;
		LoadDatabase(importer, path);
	}
	else
		std::cout << type.toStdString() << " is not a valid importer type.";
}
void QtGroundTruthBuilderApp::LoadDatabase(GTImporter* importer, const QString& path)
{
	mAppContext->clear();
	mCurrentGraphicsScene = nullptr;
	bool cancel = false;
	if(importer->askHomography())
	{
		bool loaded = false;
		QMessageBox msgDlg(this);
		msgDlg.setText("Do you want to specifiy an homography matrix ? (If this is a ground truth, No, if this is a tracker output with an homography, Yes)");
		msgDlg.setStandardButtons(QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
		int ret = msgDlg.exec();
		cv::Mat homographyMatrix = cv::Mat::eye(3,3, CV_MAKETYPE(cv::DataDepth<double>::value ,1));
		
		switch(ret) 
		{
		case QMessageBox::Yes:
			{
			QString homographyPath = QFileDialog::getOpenFileName(this,
				tr("Load homography file"), "",
				tr("All Files (*)"));

			if(homographyPath != "")
			{
				Utils::IO::LoadMatrix<double>(homographyPath.toStdString(), homographyMatrix);
				//cv::transpose(homographyMatrix, homographyMatrix);
				cv::invert(homographyMatrix,homographyMatrix);
				loaded = true;			
			}
			}
			break;
		case QMessageBox::Cancel:
			cancel = true;
			break;
		default:
			break;
		}
		importer->setHomography(homographyMatrix);		
	}
	if(!cancel)
	{
		importer->setLoadPath(path);
		runAsyncImport(importer);
	}
	
	

}



void QtGroundTruthBuilderApp::LoadDatabase()
{
	QObject* sender = QObject::sender();
	QAction* importerAction = (QAction*) sender;

	GTImporter* importer = mActionToImporter[importerAction];
	

	if(mCurrentGraphicsScene)
	{		
		QString path = "";
		if(importer->getType() == GTImporter::DIRECTORY)
			path = QFileDialog::getExistingDirectory(this, importer->getName(),	importer->getExtension(),	QFileDialog::ShowDirsOnly| QFileDialog::DontResolveSymlinks);	
		else //File
		{
			path = QFileDialog::getOpenFileName(this, importer->getName(),	"",	importer->getExtension());	
		}

		if(path != "")
		{
			LoadDatabase(importer, path);
		}
	}
}

void QtGroundTruthBuilderApp::OpenNegativeSampleDlg()
{
	NegativeSampleDlg dlg(this);

	if( dlg.exec() == QDialog::Accepted )
	{
		int minX, maxX, minY, maxY, nbSamples;
		dlg.getValues(minX,maxX, minY, maxY,nbSamples);
		ExtractNegSubImage(minX, maxX, minY, maxY, nbSamples);
	}   
}


void QtGroundTruthBuilderApp::ExtractNegSubImage(int minX, int maxX, int minY, int maxY, int maxNbObject)
{
	const std::map<unsigned int, DrawableSceneView*>& timestampToScene = mAppContext->getTimestampToScene();
	int nbNegImageByFrame = maxNbObject/timestampToScene.size();
	int maxNbTry = 100;
	int saveName = 0;

	std::string path("extracted/negobject/");
	QDir dir;
	bool success = dir.mkpath(path.c_str());

	for(auto sceneIt = timestampToScene.begin(); sceneIt != timestampToScene.end() && saveName < maxNbObject; ++sceneIt)
	{
		int nbGenerated = 0;
		int nbTry = 0;
		cv::Mat rawImage;
		if(mVfm->seekAtFramePos(sceneIt->first) && mVfm->getNextFrame(rawImage))
		{
			DrawableSceneView* dsw = sceneIt->second;
			const QList<QGraphicsItem*>& objectList = dsw->items();
			int sceneWidth = mCurrentGraphicsScene->sceneRect().right();
			int sceneHeight = mCurrentGraphicsScene->sceneRect().bottom();
			
			while(nbTry < maxNbTry && nbGenerated < nbNegImageByFrame)
			{
				int width = rand() % (maxX-minX) + minX;
				int height = rand() % (maxY-minY) + minY;
				int X1 = rand() % (sceneWidth-width);
				int Y1 = rand() % (sceneHeight-height);	
				int X2 = X1+width;
				int Y2 = Y1+height;
				bool overlap = false;
				for(auto it = objectList.begin(); it != objectList.end() ;++it)
				{
					ObjectObservation *objObs = dynamic_cast<ObjectObservation*>(*it);
					if(objObs)
					{
						QRectF rect = objObs->rect();

						int ObsY1 = rect.top() > 0 ? rect.top() : 0;
						int ObsY2 = rect.bottom() <= rawImage.rows ? rect.bottom() : rawImage.rows;
						int ObsX1 = rect.left() > 0 ? rect.left() : 0;
						int ObsX2 = rect.right() <= rawImage.cols ? rect.right() : rawImage.cols;


						if (X1 < ObsX2 && X2 > ObsX1 &&	Y1 < ObsY2 && Y2 > ObsY1) 
						{
							overlap = true;
						}					
					}
				}
				if(overlap)
				{
					++nbTry;
				}
				else
				{
					cv::Mat subPicture(rawImage, cv::Range(Y1, Y2), cv::Range(X1, X2));
					cv::imwrite(path+Utils::String::toString(saveName).c_str()+".png", subPicture);
					++nbGenerated;
					++saveName;					
				}
			}
		}
	}
}

//TODO: Add scaling to extracted subimage, RGB to gray and basic features used by classifiers
void QtGroundTruthBuilderApp::ExtractSubImage()
{
	int id = 0;
	const std::map<unsigned int, DrawableSceneView*>& timestampToScene = mAppContext->getTimestampToScene();

	for(auto sceneIt = timestampToScene.begin(); sceneIt != timestampToScene.end(); ++sceneIt)
	{
		cv::Mat rawImage;
		if(mVfm->seekAtFramePos(sceneIt->first) && mVfm->getNextFrame(rawImage))
		{
			DrawableSceneView* dsw = sceneIt->second;
			const QList<QGraphicsItem*>& objectList = dsw->items();
			if(!objectList.empty())
			{
				for(auto it = objectList.begin(); it != objectList.end() ;++it)
				{
					ObjectObservation *objObs = dynamic_cast<ObjectObservation*>(*it);
					if(objObs)
					{
						auto sceneObject = objObs->getSceneObject();
						std::string filePath = "";
						if(sceneObject)
						{
							std::string nameFolder = "";
							if(!mUi.actionSave_Extracted_objects_by_Type)
							{
								nameFolder = sceneObject->getDescription()+" "+sceneObject->getId()+"/";
							}

							std::string path = "extracted/"+sceneObject->getType()+"/"+nameFolder;
							QDir dir;
							bool success = dir.mkpath(QString(path.c_str()));
							if(success)
							{					
								filePath = path+sceneObject->getDescription()+sceneObject->getId()+" "+Utils::String::toString(sceneIt->first)+".png";								
							}
						}	
						else
						{
							std::string path = "extracted/noobject/";
							QDir dir;
							bool success = dir.mkpath(QString(path.c_str()));
							
							filePath = path + Utils::String::toString(id++) +".png";		
						}
						QRectF rect = objObs->rect();

						int top = rect.top() > 0 ? rect.top() : 0;
						int bottom = rect.bottom() <= rawImage.rows ? rect.bottom() : rawImage.rows;
						int left = rect.left() > 0 ? rect.left() : 0;
						int right = rect.right() <= rawImage.cols ? rect.right() : rawImage.cols;

						cv::Mat subPicture(rawImage, cv::Range(top, bottom), cv::Range(left, right));
						cv::imwrite(filePath, subPicture);
					}
					
				}		
			}
		}
	}
	//This should be a class with QProgressDialog
}

void QtGroundTruthBuilderApp::updateGraphicsViewScale()
{
	if (!mCurrentFrameMat.empty())
	{
		int width = mUi.CurrentFrameWidget->width();
		int height = mUi.CurrentFrameWidget->height();
		float maxScale = std::min((float)width / (float)mCurrentFrameMat.cols, (float)height / (float)mCurrentFrameMat.rows);
		float relativeScale = maxScale / mCurrentGraphicsViewScaleFactor;
		mCurrentGraphicsViewScaleFactor = maxScale;
		mUi.CurrentFrameWidget->scale(relativeScale, relativeScale);
	}
}
void QtGroundTruthBuilderApp::seekToFrame(int fNumber)
{
	if(fNumber>=0 && mVfm)
	{
		cv::Mat tmpFrame;
		if(mVfm->seekAtFramePos(fNumber) && mVfm->getNextFrame(tmpFrame))
		{
			mCurrentFrameMat = tmpFrame.clone();
			cv::cvtColor(tmpFrame, tmpFrame, CV_BGR2RGB);
			QImage img = QImage(tmpFrame.data,
				tmpFrame.cols,
				tmpFrame.rows,
				tmpFrame.step,
				QImage::Format_RGB888);
	
			if(mInitOnFirstFrame)
			{
				updateGraphicsViewScale();


				//resize(tmpFrame.cols, tmpFrame.rows);
				mUi.CurrentFrameWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
				mUi.CurrentFrameWidget->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
				mUi.CurrentFrameWidget->setDragMode(QGraphicsView::NoDrag);
				mInitOnFirstFrame = false;
			}	

			auto it = mAppContext->getTimestampToScene().find(fNumber);
			if(it == mAppContext->getTimestampToScene().end())
			{
				auto itPair = mAppContext->getTimestampToScene().insert(std::pair<unsigned int, DrawableSceneView*>(fNumber, new DrawableSceneView(fNumber, this, mAppContext)));
				it = itPair.first;
			}
			std::set<SceneObject*> currentlySelectedObjects;
			if(mCurrentGraphicsScene)
			{
				
				auto list = mCurrentGraphicsScene->selectedItems();
				for(auto it = list.begin(); it != list.end() ;++it)
				{
					if((*it)->isSelected())
					{
						ObjectObservation* objObs = dynamic_cast<ObjectObservation*>(*it);
						if(objObs)						
							currentlySelectedObjects.insert(objObs->getSceneObject());	
						
						(*it)->setSelected(false);						
					}					
				}
			}
			if(mCurrentGraphicsScene)
			{
				disconnect(mCurrentGraphicsScene, SIGNAL(selectionChanged()), this, SLOT(SceneObjectSelectionChanged())); 
				disconnect(mCurrentGraphicsScene, SIGNAL(selectionChanged()), this, SLOT(UpdateCurrentFrameObjectList())); 
				disconnect(mCurrentGraphicsScene, SIGNAL(selectionChanged()), this, SLOT(UpdateObjectIdList())); 
				QList<QGraphicsItem*> itemList = mCurrentGraphicsScene->items();
				for (QGraphicsItem* item : itemList)
				{
					QGraphicsPixmapItem* pixmap = dynamic_cast<QGraphicsPixmapItem*>(item);
					if (pixmap)
					{
						mCurrentGraphicsScene->removeItem(pixmap); //This will take way too much memory so we will readd it when seeking in the video
						delete pixmap;
					}
				}

			}
			mCurrentGraphicsScene = (*it).second;
			
			//We reselect the associated object
			auto objectList = mCurrentGraphicsScene->items();
			for(auto it = objectList.begin(); it != objectList.end() ;++it)
			{
				ObjectObservation* objObs = dynamic_cast<ObjectObservation*>(*it);
				if(objObs && currentlySelectedObjects.find(objObs->getSceneObject()) != currentlySelectedObjects.end())				
					(*it)->setSelected(true);	
			}

			
			mUi.CurrentFrameWidget->setScene(mCurrentGraphicsScene);
			mCurrentGraphicsScene->setSceneRect(0,0, tmpFrame.cols,tmpFrame.rows);
			QGraphicsPixmapItem* item = mCurrentGraphicsScene->addPixmap(QPixmap::fromImage(img));
			item->setZValue(-1);
			bool connected = connect(mCurrentGraphicsScene, SIGNAL(selectionChanged()), this, SLOT(SceneObjectSelectionChanged())); 
			connect(mCurrentGraphicsScene, SIGNAL(selectionChanged()), this, SLOT(UpdateCurrentFrameObjectList())); 
			connect(mCurrentGraphicsScene, SIGNAL(selectionChanged()), this, SLOT(UpdateObjectIdList())); 
	
			assert(connected);
			//mUi.CurrentFrameWidget->fitInView(mCurrentGraphicsScene->sceneRect(), Qt::KeepAspectRatio);

			//mUi.CurrentFrameWidget->setFixedSize(tmpFrame.cols,	tmpFrame.rows);
			mUi.CurrentVideoSeekPosLabel->setText(QString::number(fNumber) + " / " + QString::number(mVfm->getNbFrame()-1));
			emit OnFrameChange();
		}
	}	
}

void QtGroundTruthBuilderApp::resizeEvent(QResizeEvent* event)
{
	QMainWindow::resizeEvent(event);
	updateGraphicsViewScale();

}

SceneObject* QtGroundTruthBuilderApp::getFrameListCurrentObject()
{
	SceneObject* so = nullptr;
	QString name = mUi.ObjectIdList->itemText(mUi.ObjectIdList->currentIndex());
	QVariant v = mUi.ObjectIdList->itemData(mUi.ObjectIdList->currentIndex(), Qt::UserRole);
	if(v.value<void *>() != nullptr)
	{
		so = (SceneObject*) v.value<void *>();
	}
	return so;
}


void QtGroundTruthBuilderApp::UpdateCurrentFrameObjectList()
{
	if(mCurrentGraphicsScene)
	{
		mUi.CurrentFrameObjetListWidget->clear();
		auto list = mCurrentGraphicsScene->items();
		if(!list.empty())
		{
			for(auto it = list.begin(); it != list.end() ;++it)
			{
				ObjectObservation *object = dynamic_cast<ObjectObservation*>(*it);
				if (object != nullptr && object->getSceneObject())
				{		
					std::string displayName = object->getSceneObject()->getId();
					if(!object->getSceneObject()->getDescription().empty())
					displayName+= " - " + object->getSceneObject()->getDescription();
					QListWidgetItem* item = new QListWidgetItem(QString(displayName.c_str()), mUi.CurrentFrameObjetListWidget);
					item->setData(Qt::UserRole, QVariant::fromValue((void*)(object)));
					if(object->isSelected())
						item->isSelected();
					//
				}
			}		
		}
		mUi.CurrentFrameObjetListWidget->sortItems(Qt::AscendingOrder);
	}
}

QString QtGroundTruthBuilderApp::AddNewObjectType()
{	
	QString objectType = QInputDialog::getText(this, tr("Set name of the new object type"), tr("Object type : "));
	if(objectType != "")
	{
		mAppContext->getObjectTypeManager().addType(objectType.toStdString());
		this->UpdateObjectTypeList();
	}
	return objectType;
}

void QtGroundTruthBuilderApp::SceneObjectSelectionChanged()
{
	bool enableControls = false;
	if(mCurrentGraphicsScene)
	{
		auto list = mCurrentGraphicsScene->selectedItems();

		if(!list.empty())
		{
			ObjectObservation* obs = dynamic_cast<ObjectObservation*>(list[0]);
			SceneObject* sceneObj = obs->getSceneObject();
			if(sceneObj)
			{
				int objectIdx = mAppContext->getObjectIdx(sceneObj);
				mUi.ObjectIdList->setCurrentIndex(objectIdx);
				
				mUi.objectDescriptionEdit->setText(sceneObj->getDescription().c_str());	
				int typeIdx = mAppContext->getObjectTypeManager().getIdx(sceneObj->getType());
				mUi.ObjectTypeList->setCurrentIndex(typeIdx);
				disconnect(mUi.CurrentFrameObjetListWidget, SIGNAL(itemSelectionChanged()), this, SLOT(onListItemSelectionChange()));
				for(unsigned int itemIdx = 0; itemIdx < mUi.CurrentFrameObjetListWidget->count(); ++itemIdx)
				{
					QVariant data = mUi.CurrentFrameObjetListWidget->item(itemIdx)->data(Qt::UserRole);
					ObjectObservation* object = (ObjectObservation*)data.value<void*>();
					if(object == obs)
						mUi.CurrentFrameObjetListWidget->item(itemIdx)->setSelected(true);
					else
						mUi.CurrentFrameObjetListWidget->item(itemIdx)->setSelected(false);
		
				}	
				connect(mUi.CurrentFrameObjetListWidget, SIGNAL(itemSelectionChanged()), this, SLOT(onListItemSelectionChange()));


			}
			enableControls = true;

		}
		else
		{			
			mUi.objectDescriptionEdit->setText("");
		}
	}	
	mUi.objectDescriptionEdit->setEnabled(enableControls);
}

void QtGroundTruthBuilderApp::GoToNextFrame()
{
	unsigned int frameNb = mUi.FrameStep->text().toUInt();	
	if(mVfm)
	{
		int fNumber = mVfm->getNextFramePos()+frameNb-1;
		
		//Skip blank image
		if(mUi.goToNextFrameCb->isChecked())
		{
			const auto& timestampToSceneMap = mAppContext->getTimestampToScene();
			auto it = timestampToSceneMap.lower_bound(fNumber);
			if(it != timestampToSceneMap.end())
				fNumber = it->first;
		}
		seekToFrame(fNumber);
	}
}

void QtGroundTruthBuilderApp::GoToPreviousFrame()
{
	unsigned int frameNb = mUi.FrameStep->text().toUInt();	
	if(mVfm)
	{
		int fNumber = mVfm->getNextFramePos()-frameNb-1;
		
		//Skip blank image
		if(mUi.goToNextFrameCb->isChecked())
		{
			auto timestampToSceneMap = mAppContext->getTimestampToScene();
			auto it =  timestampToSceneMap.find(fNumber);
			bool found = it != timestampToSceneMap.end();
			while(!found && fNumber >  0)
			{
				it =  timestampToSceneMap.find(--fNumber);
				if(it != timestampToSceneMap.end() && !(*it).second->items().empty())
				{
					for (int i = 0; i < (*it).second->items().size() &&!found; ++i)
					{
						if(dynamic_cast<ObjectObservation*>((*it).second->items()[i]))
							found = true;
					}
				}
			}
		}
		seekToFrame(fNumber);
	}
}
