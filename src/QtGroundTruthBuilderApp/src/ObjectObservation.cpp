#include "ObjectObservation.h"

#include "BoundingBox.h"
#include "SceneObject.h"

#include <QBrush>
#include <QPen>
#include <QPainter>
#include <QGraphicsSceneHoverEvent>
#include <QPointF>

ObjectObservation::ObjectObservation(QColor color, bool interpolated)
: QGraphicsRectItem()
, mHoverPoint(-1)
, mSelectedPoint(-1)
, mHandleSize(6,6)
, mColor(color)
, mObject(nullptr)
, mInterpolated(interpolated)
{
	setFlags(QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsFocusable | QGraphicsItem::ItemIsSelectable);
	setAcceptHoverEvents(true);
}

ObjectObservation::~ObjectObservation()
{

}

void ObjectObservation::associateToSceneObject( SceneObject* object )
{
	Color c(0,0,0);
	if(object)
		c = object->getColor();
	setColor(c.R, c.G, c.B);
	mObject = object;
}





// Inspir� en partie de : http://www.qtfr.org/viewtopic.php?id=8827
void ObjectObservation::paint(QPainter* painter, const QStyleOptionGraphicsItem*,  QWidget*) 
{
	QRectF rect = this->rect();
	if(isSelected())
	{
		QPen selPen=QPen(Qt::DashLine);  
		selPen.setColor(Qt::black);  
		selPen.setWidth(2);
		QBrush selBrush=QBrush(Qt::NoBrush);  
		painter->setBrush(selBrush);  
		painter->setPen(selPen); 
		// Rect  
		painter->drawRect(QRectF(rect.x(),rect.y(),rect.width(),rect.height()));  
		// Draghandles  
		selBrush=QBrush(Qt::red,Qt::SolidPattern);  
		selPen=QPen(Qt::red);  
		painter->setBrush(selBrush);  
		painter->setPen(selPen);  
		QPointF point;  
		for(int i=0;i<8;i++)  
		{  

			if(i<3)
				point=QPointF(rect.x()+rect.width()/2*i,rect.y());  
			if(i==3) 
				point=QPointF(rect.x()+rect.width(),rect.y()+rect.height()/2);  
			if(i>3 && i<7) 
				point=QPointF(rect.x()+rect.width()/2*(i-4),rect.y()+rect.height());  
			if(i==7) 
				point=QPointF(rect.x(),rect.y()+rect.height()/2);  
			if(i==mHoverPoint) 
				painter->setBrush(QBrush(Qt::blue,Qt::SolidPattern));  

			// Rect around valid point  
			painter->drawRect(QRectF(point-mHandleSize,point+mHandleSize));  

			if(i==mHoverPoint)
				painter->setBrush(selBrush);  
		}  
	}  
	else
	{
		QPen selPen=QPen(Qt::SolidPattern);  
		selPen.setColor(mColor);
		selPen.setWidth(2);
		painter->setBrush(QBrush(Qt::NoBrush));  
		painter->setPen(selPen); 
		painter->drawRect(QRectF(rect.x(),rect.y(),rect.width(),rect.height()));  
		//We draw the id
		SceneObject* so = getSceneObject();
		if(so)
		{
			QFont font = painter->font();
			font.setPointSize(16);
			painter->setFont(font);
			QRectF position = rect;
			position.setTopLeft(QPointF(position.x()+2, position.y()+2));
			std::string id = so->getId();
			if(mInterpolated)
				id +="-I";
			painter->drawText(position, id.c_str());
		}				
	}
}

void ObjectObservation::hoverLeaveEvent(QGraphicsSceneHoverEvent*) 
{  
	mHoverPoint = -1;
	redrawItem();
}

QRectF ObjectObservation::boundingRect() const  
{  
	QRectF rect = this->rect();

	rect = QRectF(rect.x()-mHandleSize.x(), rect.y()-mHandleSize.y(), rect.width()+mHandleSize.x()*2, rect.height()+mHandleSize.y()*2);
	
	return rect;
}  


void ObjectObservation::hoverMoveEvent(QGraphicsSceneHoverEvent *e) 
{  
	if (isSelected()) 
	{  
		
		QRectF rect = this->rect();

		QPointF hover_point = e->pos();  
		QPointF point;  
		for(mHoverPoint=0; mHoverPoint<8; mHoverPoint++)
		{  
			if(mHoverPoint<3) 
				point=QPointF(rect.x()+rect.width()/2*mHoverPoint,rect.y());  
			if(mHoverPoint==3) 
				point=QPointF(rect.x()+rect.width(),rect.y()+rect.height()/2);  
			if(mHoverPoint>3 && mHoverPoint<7) 
				point=QPointF(rect.x()+rect.width()/2*(mHoverPoint-4),rect.y()+rect.height());  
			if(mHoverPoint==7) 
				point=QPointF(rect.x(), rect.y()+rect.height()/2);  
			if(hasClickedOn(hover_point,point)) 
				break;  
		} 
		if(mHoverPoint==8) 
			mHoverPoint=-1;  
		else 
			redrawItem(); 
	}  
	else
		mHoverPoint = -1;

	QGraphicsItem::hoverMoveEvent(e);  
}  

void ObjectObservation::mousePressEvent(QGraphicsSceneMouseEvent *e) 
{  
	mLastPoint = e->scenePos ();
	if(isSelected()){ 
		mInterpolated = false;
		QRectF rect = this->rect();
		if (e -> buttons() & Qt::LeftButton) {  
			QPointF mouse_point = (e -> pos());  
			QPointF point;  
			for(mSelectedPoint=0;mSelectedPoint<8;mSelectedPoint++)
			{  
				point = rect.topLeft();
				if(mSelectedPoint<3) 
					point+=QPointF(rect.width()/2*mSelectedPoint,0);  
				if(mSelectedPoint==3) 
					point+=QPointF(rect.width(),rect.height()/2);  
				if(mSelectedPoint>3 && mSelectedPoint<7) 
					point+=QPointF(rect.width()/2*(mSelectedPoint-4),rect.height());  
				if(mSelectedPoint==7) 
					point+=QPointF(0,rect.height()/2);  
				if(hasClickedOn(mouse_point,point)) 
					break;  
			}//for  
			if(mSelectedPoint==8) 
				mSelectedPoint=-1;  
			else e->accept();  
		}  
		redrawItem();
		

	}  
	QGraphicsItem::mousePressEvent(e);  
}  

void ObjectObservation::mouseReleaseEvent(QGraphicsSceneMouseEvent *e)
{
	
	QGraphicsItem::mouseReleaseEvent(e);  
}

void ObjectObservation::mouseMoveEvent(QGraphicsSceneMouseEvent *e) 
{  
	mInterpolated = false;
	// left click  
	bool leftClick = (e -> buttons() & Qt::LeftButton);

	if(leftClick)
	{
		if(mSelectedPoint == -1)
		{
			prepareGeometryChange();  
			QRectF rect = this->rect();
			QPointF newPos = e->scenePos();

			if(newPos != mLastPoint && mSelectedPoint==-1)
			{
				QRectF rect = this->rect();
				rect.translate(newPos-mLastPoint);
				setRect(rect);
			}
			mLastPoint = newPos;
			//rect.moveBottomRight(e->po
			//QGraphicsItem::mouseMoveEvent(e); 
			update();
			//rect = this->rect();
			//std::cout << rect.x() << " " << rect.y() << " " << rect.width() << " " << rect.height() << std::endl;
			
		}
		else
		{  		
			QPointF mouse_point = e -> pos();  
			QRectF rect = this->rect();

			switch (mSelectedPoint) {  
			case TOPLEFTHANDLE:  //TopLeftCorner
				{
					bool y = false;
					bool x = false;
					if(mouse_point.y() > rect.bottom())
					{
						y = true;
						rect.setBottom(mouse_point.y());
					}
					else
						rect.setTop(mouse_point.y());

					if(mouse_point.x() > rect.right())
					{
						x = true;
						rect.setRight(mouse_point.x());
					}
					else
						rect.setLeft(mouse_point.x());  
					if(x && y)
						mSelectedPoint = BOTTOMRIGHTHANDLE;
					else if(x)
						mSelectedPoint = TOPRIGHTHANDLE;
					else if(y)
						mSelectedPoint = BOTTOMLEFTHANDLE;				

				}			
				break;  
			case TOPHANDLE: //Top
				{
					if(mouse_point.y() > rect.bottom())
					{
						mSelectedPoint = BOTTOMHANDLE;
						rect.setBottom(mouse_point.y());
					}
					else
						rect.setTop(mouse_point.y());
				}

				break;  
			case TOPRIGHTHANDLE:  //TopRight
				{
					bool y = false;
					bool x = false;
					if(mouse_point.y() > rect.bottom())
					{
						y = true;
						rect.setBottom(mouse_point.y());
					}
					else
						rect.setTop(mouse_point.y());

					if(mouse_point.x() < rect.left())
					{
						x = true;
						rect.setLeft(mouse_point.x());
					}
					else
						rect.setRight(mouse_point.x());  
					if(x && y)
						mSelectedPoint = BOTTOMLEFTHANDLE;
					else if(x)
						mSelectedPoint = TOPLEFTHANDLE;
					else if(y)
						mSelectedPoint = BOTTOMLEFTHANDLE;
				}



				break;  
			case RIGHTHANDLE: //Right
				{
					if(mouse_point.x() < rect.left())
					{
						mSelectedPoint = LEFTHANDLE;
						rect.setLeft(mouse_point.x());
					}
					else
						rect.setRight(mouse_point.x());  
				}
				break;  
			case BOTTOMRIGHTHANDLE:   //Bottom Right
				{
					bool y = false;
					bool x = false;
					if(mouse_point.y() < rect.top())
					{
						y = true;
						rect.setTop(mouse_point.y());
					}
					else
						rect.setBottom(mouse_point.y());

					if(mouse_point.x() < rect.left())
					{
						x = true;
						rect.setLeft(mouse_point.x());
					}
					else
						rect.setRight(mouse_point.x());  
					if(x && y)
						mSelectedPoint = TOPLEFTHANDLE;
					else if(x)
						mSelectedPoint = BOTTOMLEFTHANDLE;
					else if(y)
						mSelectedPoint = TOPRIGHTHANDLE;
				}




				break;  
			case BOTTOMHANDLE:  //Bottom
				{
					if(mouse_point.y() < rect.top())
					{
						mSelectedPoint = TOPHANDLE;
						rect.setTop(mouse_point.y());
					}
					else
						rect.setBottom(mouse_point.y()); 
				}

				break;  
			case BOTTOMLEFTHANDLE:  //BottomLeft
				{
					bool y = false;
					bool x = false;
					if(mouse_point.y() < rect.top())
					{
						y = true;
						rect.setTop(mouse_point.y());
					}
					else
						rect.setBottom(mouse_point.y());

					if(mouse_point.x() > rect.right())
					{
						x = true;
						rect.setRight(mouse_point.x());
					}
					else
						rect.setLeft(mouse_point.x());  
					if(x && y)
						mSelectedPoint = TOPRIGHTHANDLE;
					else if(x)
						mSelectedPoint = BOTTOMRIGHTHANDLE;
					else if(y)
						mSelectedPoint = TOPLEFTHANDLE;
				}


				break;  
			case LEFTHANDLE:  //Left
				if(mouse_point.x() > rect.right())
				{
					mSelectedPoint = RIGHTHANDLE;
					rect.setRight(mouse_point.x());
				}
				else
					rect.setLeft(mouse_point.x());  

				break;  
			default:  
				break;  
			}  
			mHoverPoint = mSelectedPoint;
			rect = getRectangle(rect.topLeft(), rect.bottomRight());
			setRect(rect);
			redrawItem();
		} 
	}
	else
		QGraphicsItem::mouseMoveEvent(e); 
}  

void ObjectObservation::setBoundingBox(QPointF topLeftCorner, QPointF bottomRightCorner)
{
	QRectF rec = getRectangle(topLeftCorner, bottomRightCorner);
	setRect(rec);
}


QRectF ObjectObservation::getRectangle(const QPointF& p1, const QPointF& p2) const
{

	float xMin = p1.x();
	float xMax = p2.x();
	float yMin = p1.y();
	float yMax = p2.y();

	if(xMin > xMax)
	{
		xMin = p2.x();
		xMax = p1.x();
	}

	if(yMin > yMax)
	{
		yMin = p2.y();
		yMax = p1.y();
	}


	return QRectF(QPointF(xMin, yMin), QPointF(xMax, yMax));
}


bool ObjectObservation::hasClickedOn(QPointF p1, QPointF p2)
{
	float deltaX = p1.x()-p2.x();
	float deltaY = p1.y()-p2.y();
	float dist = sqrt(deltaX*deltaX+deltaY*deltaY);
	return  dist < 2*sqrt(mHandleSize.x()*mHandleSize.x()+mHandleSize.y()*mHandleSize.y());
}

void ObjectObservation::redrawItem()
{
	QRectF rect = this->rect();
	update(0,0,800,600);
	//update(rect.x()-mHandleSize.x()*5, rect.y()-mHandleSize.y()*5, rect.width()+mHandleSize.x()*5, rect.height()+mHandleSize.y()*5);
}
