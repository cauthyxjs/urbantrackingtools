#include "PolyTrackExporter.h"

#include "StatusProgressBar.h"
#include "SQLiteManager.h"
#include "Logger.h"
#include "SceneObject.h"
#include <set>
#include <map>
#include "DrawableSceneView.h"
#include "AppContext.h"
#include "ObjectObservation.h"

PolyTrackExporter::PolyTrackExporter(StatusProgressBar* progressBarControl, AppContext* context)
: ExporterTask(progressBarControl, context, "Polytrack", "*.sqlite")
, mSQLManager(nullptr)
{

}

PolyTrackExporter::~PolyTrackExporter()
{

}

void PolyTrackExporter::run()
{
	mSQLManager = new SQLiteManager(mFilePath);
	if(mSQLManager->isConnected())
	{
		dropTables();
		createTables();
		saveCurrentContext();
	}
	else
	{
		//Log Error
	}
	
	delete mSQLManager;
}

bool PolyTrackExporter::createTables()
{
	bool success = mSQLManager->beginTransaction();
	success &= mSQLManager->executeStatement("create table objects_type ( road_user_type INTEGER, type_string TEXT, PRIMARY KEY( road_user_type) )");
	success &= mSQLManager->executeStatement("create table objects ( object_id INTEGER, road_user_type INTEGER, description TEXT, PRIMARY KEY( object_id) )");
	success &= mSQLManager->executeStatement("create table bounding_boxes ( object_id INTEGER, frame_number INTEGER, x_top_left REAL, y_top_left REAL, x_bottom_right REAL, y_bottom_right REAL,  PRIMARY KEY( object_id, frame_number ) )");
	success &= mSQLManager->endTransaction();
	return success;
}

bool PolyTrackExporter::dropTables()
{
	bool success = mSQLManager->beginTransaction();
	success &= mSQLManager->executeStatement("drop table if exists bounding_boxes;" );
	success &= mSQLManager->executeStatement("drop table if exists objects;" );
	success &= mSQLManager->executeStatement("drop table if exists objects_type;");
	success &= mSQLManager->endTransaction();
	return success; 
}





bool PolyTrackExporter::saveCurrentContext()
{
	//Sort and rename object
	cleanObjectsId();
	bool success = saveObjectTypeTable();
	success &= saveSceneObjectTable();
	this->updateProgressBarPercent(10);
	success &= saveBoundingBox();	
	return success;
}


bool PolyTrackExporter::saveSceneObjectTable()
{
	bool success = mSQLManager->beginTransaction();
	std::vector<SceneObject*>& sceneObjectList = mContext->getSceneObjectList();

	for(auto it = sceneObjectList.begin(); it != sceneObjectList.end();++it)
	{
		
		success &= mSQLManager->executeStatement("insert into objects (object_id, road_user_type, description) values ('" + (*it)->getId() + "', '" +  Utils::String::toString(mContext->getObjectTypeManager().getIdx((*it)->getType())) + "', '" + (*it)->getDescription() + "');");
		if(!success)
		{
			LOGERROR("Error saving object " << (*it)->getId());
			success = true;
		}
	}

	success &= mSQLManager->endTransaction();
	return success;
}

bool PolyTrackExporter::saveObjectTypeTable()
{
	bool success = mSQLManager->beginTransaction();
	const ObjectTypeManager& manager = mContext->getObjectTypeManager();
	auto typeList = manager.getTypeList();
	std::stringstream ss;
	for(auto it = typeList.begin(); it != typeList.end();++it)
		ss << "insert into objects_type (road_user_type, type_string) values ('" << (*it).first << "', '" <<(*it).second << "');";
	success &= mSQLManager->executeStatement(ss.str());
	success &= mSQLManager->endTransaction();
	return success;
}

bool PolyTrackExporter::saveBoundingBox()
{
	bool success = mSQLManager->beginTransaction();
	std::map<unsigned int, DrawableSceneView*>& timestampToScene= mContext->getTimestampToScene();
	int nbInc = 0;
	typedef std::string ObjectId;
	typedef std::set<int> TimestampSet;
	std::map<ObjectId, TimestampSet> objIdToBoundingBoxFrameSet;
	for (auto it = timestampToScene.begin(); it != timestampToScene.end();++it)
	{		
		updateProgressBarPercent(10+90.f*((float)nbInc/timestampToScene.size()));
		++nbInc;
		std::stringstream ss;
		unsigned int timestamp = (*it).first;
		DrawableSceneView* sceneView = (*it).second;
		QList<QGraphicsItem *> items = sceneView->items();
				
		for (int i = 0; i < items.size(); ++i)
		{
			ObjectObservation* obj = dynamic_cast<ObjectObservation*>(items[i]);
			if(obj)
			{
				QRectF rect = obj->rect();
				std::string objId = Utils::String::toString(i); //This id is reserved
				if(obj->getSceneObject())
					objId = obj->getSceneObject()->getId();

				int x1 = rect.topLeft().x() >= 0 ? rect.topLeft().x():0;
				x1 = x1 < mWidth ? x1 : mWidth-1;
				int y1 = rect.topLeft().y() >= 0 ? rect.topLeft().y():0;
				y1 = y1 < mHeight ? y1 : mHeight-1;
				int x2 = rect.bottomRight().x() < mWidth ? rect.bottomRight().x() : mWidth-1;
				x2 = x2 >=0 ? x2 : 0;
				int y2 = rect.bottomRight().y() < mHeight ? rect.bottomRight().y() : mHeight-1;
				y2 = y2 >=0 ? y2 : 0;
				int w = x2 - x1;
				int h = y2 - y1;
				if(w<0)
				{
					auto tmp = x2;
					x2 = x1;
					x1 = tmp;
				}
					
				if(h<0)
				{
					auto tmp = y2;
					y2 = y1;
					y1 = tmp;
				}
				
				if(h != 0 && w != 0)
				{
					ss << "insert into bounding_boxes (object_id, frame_number, x_top_left, y_top_left, x_bottom_right, y_bottom_right) values ('"
					<< objId << "', " << timestamp <<", " << x1 << ", " << y1 << ", "
					<< x2 << ", " << y2 << ");";
					objIdToBoundingBoxFrameSet[objId].insert(timestamp);
				}				
			}
		}	
		success &= mSQLManager->executeStatement(ss.str());
		if(!success)
		{
			LOGERROR("Error at " << timestamp);
			success = true;
		}
	}
	//Verify continuity
	for(auto objInstanceIt = objIdToBoundingBoxFrameSet.begin(); objInstanceIt != objIdToBoundingBoxFrameSet.end(); ++objInstanceIt)
	{
		const std::string& objId = objInstanceIt->first;
		std::set<int>& timestamps =  objInstanceIt->second;
		if(timestamps.size() > 1)
		{
			int startTimestamp = *timestamps.begin();
			int endTimestamp = *timestamps.rbegin(); 
			if(endTimestamp+1-startTimestamp != timestamps.size())
			{
				int previousTimestamp = startTimestamp-1; 
				for(auto timestampIt =  timestamps.begin(); timestampIt != timestamps.end(); ++timestampIt)
				{
					if(*timestampIt != previousTimestamp+1)
					{
						LOGWARNING("Missing annotations for object " << objId << "  for frames between (" << previousTimestamp <<", " << *timestampIt << ").");
					}
					previousTimestamp = *timestampIt;
				}
			}

		}

	}


	success &= mSQLManager->endTransaction();
	return success;
}