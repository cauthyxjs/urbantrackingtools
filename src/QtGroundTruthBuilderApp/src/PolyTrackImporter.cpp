#include "PolyTrackImporter.h"



#include "SQLiteManager.h"
#include "AppContext.h"
#include "SceneObject.h"
#include <map>
#include <iostream>
#include "ObjectObservation.h"
#include "DrawableSceneView.h"
#include "InputVideoFileModule.h"
#include "OpenCVHelpers.h"
PolyTrackImporter::PolyTrackImporter(StatusProgressBar* progressBarControl, AppContext* appContext, unsigned int videoWidth, unsigned int videoHeight)
: GTImporter(progressBarControl, appContext, "PolyTrack", "*.sqlite", "Polytrack SQLite file" , GTImporter::FILE, videoWidth, videoHeight)
{

}


PolyTrackImporter::~PolyTrackImporter()
{

}

bool PolyTrackImporter::LoadObjectTypeTable(SQLiteManager& sqlMgr)
{
	std::vector<std::vector<std::string>> objects;
	bool success = sqlMgr.executeStatementGetArray("SELECT road_user_type, type_string FROM objects_type", objects);
	if(success && !objects.empty() && objects[0].size() == 2)
	{
		for (unsigned int row = 0; row <objects.size(); ++row)
		{
			int typeKey;
			std::string  objectType;
			success &= Utils::String::StringToType(objects[row][0], typeKey);
			objectType = objects[row][1];
			getAppContext()->getObjectTypeManager().addType(typeKey, objectType);
		}
	}
	return success;
}

bool PolyTrackImporter::LoadObjectsTable(SQLiteManager& sqlMgr)
{
	std::vector<std::vector<std::string>> objects;
	bool descriptionExist = sqlMgr.tableFieldExist("objects", "description");
	std::string query = descriptionExist ? "SELECT object_id, road_user_type, description FROM objects" : "SELECT object_id, road_user_type FROM objects";
	std::vector<SceneObject*>& sceneObjectList = getAppContext()->getSceneObjectList();
	bool success = sqlMgr.executeStatementGetArray(query, objects);
	if(success && !objects.empty() && (objects[0].size() == 2 || objects[0].size() == 3))
	{
		for (unsigned int row = 0; row <objects.size(); ++row)
		{

			int roadUserType;
			success &= Utils::String::StringToType(objects[row][1], roadUserType);
			std::string description(descriptionExist ? objects[row][2] : "");
			if(roadUserType == -1) //TrafficIntelligence -1 is our 0 'Undefined'
				roadUserType = 0;
			const std::string& typeName = getAppContext()->getObjectTypeManager().getName(roadUserType);
			if(getAppContext()->getObjectTypeManager().getIdx(typeName) == -1)
				getAppContext()->getObjectTypeManager().addType(typeName);


			SceneObject* so = new SceneObject(objects[row][0], description, typeName);
			sceneObjectList.push_back(so);	


			mObjectIdToObjectMap.insert(std::pair<std::string, SceneObject*>(objects[row][0], so));
		}
	}
	return success;
}

bool PolyTrackImporter::LoadBoundingBoxesTable(SQLiteManager& sqlMgr)
{
	std::vector<std::vector<std::string>> objectObs;
	bool success = sqlMgr.executeStatementGetArray("SELECT object_id, frame_number, x_top_left, y_top_left, x_bottom_right, y_bottom_right FROM bounding_boxes", objectObs);
	if(success && !objectObs.empty() && objectObs[0].size() == 6)
	{
		for (unsigned int row = 0; row <objectObs.size(); ++row)
		{
			const std::string& object_id = objectObs[row][0];
			unsigned int frame_number;
			double top_left_corner_x, top_left_corner_y, bottom_right_corner_x, bottom_right_corner_y;
			success &= Utils::String::StringToType(objectObs[row][1], frame_number);
			success &= Utils::String::StringToType(objectObs[row][2], top_left_corner_x);
			success &= Utils::String::StringToType(objectObs[row][3], top_left_corner_y);
			success &= Utils::String::StringToType(objectObs[row][4], bottom_right_corner_x);
			success &= Utils::String::StringToType(objectObs[row][5], bottom_right_corner_y);
			auto it = getAppContext()->getTimestampToScene().find(frame_number);
			if(it == getAppContext()->getTimestampToScene().end())
			{
				auto itPair = getAppContext()->getTimestampToScene().insert(std::pair<unsigned int, DrawableSceneView*>(frame_number, new DrawableSceneView(frame_number, nullptr, getAppContext())));
				it = itPair.first;
			}

			ObjectObservation* objObs = new ObjectObservation(QColor(rand()%255,rand()%255, rand()%255), false);
			objObs->setRect(QRectF(top_left_corner_x, top_left_corner_y, bottom_right_corner_x-top_left_corner_x, bottom_right_corner_y-top_left_corner_y));


			SceneObject* so = nullptr;
			auto objectIt = mObjectIdToObjectMap.find(object_id);
			if(objectIt != mObjectIdToObjectMap.end())	
			{
				so = objectIt->second;				
				objObs->associateToSceneObject(so);
				DrawableSceneView* scene = it->second;
				scene->addItem(objObs);
			}
			else
			{
				success = false;
				LOGERROR("Unknown object id " << object_id);
			}
		}
	}
	return success;
}


bool PolyTrackImporter::LoadPointsTable(SQLiteManager &sqlMgr)
{
	typedef std::map<int, cv::Point> TimestampToPoint; 
	typedef std::map<std::string, TimestampToPoint> TrajIdToPointList;

	TrajIdToPointList trajectories;

	std::vector<std::vector<std::string>> positions;
	bool success = sqlMgr.executeStatementGetArray("SELECT trajectory_id, frame_number, x_coordinate, y_coordinate FROM positions", positions);
	
	if(success && !positions.empty() && positions[0].size() == 4)
	{
		for (unsigned int row = 0; row <positions.size(); ++row)
		{
			updatePercent(30+40*(float)row/positions.size());
			int frame_number;
			double xPos, yPos;
			std::string trajId = positions[row][0];

			success &= Utils::String::StringToType(positions[row][1], frame_number);
			success &= Utils::String::StringToType(positions[row][2], xPos);
			success &= Utils::String::StringToType(positions[row][3], yPos);
			TimestampToPoint& pl = trajectories[trajId];

			cv::Point p(xPos, yPos);
			if(!getHomography().empty())
			{

				std::vector<cv::Point2f> beforeHomography;
				beforeHomography.push_back(cv::Point2f(xPos, yPos));

				std::vector<cv::Point2f> afterHomography;
				cv::perspectiveTransform(beforeHomography,afterHomography, getHomography());

				pl[frame_number] = afterHomography[0];
			}
			else
				pl[frame_number] = p;			
		}
	}

	std::map<std::string, std::vector<TimestampToPoint>> ObjectIdtoPointList;




	//Load TrajectoryId/ObjectId association
	std::vector<std::vector<std::string>> objects_features;
	success &= sqlMgr.executeStatementGetArray("SELECT object_id, trajectory_id FROM objects_features", objects_features);

	if(success && !objects_features.empty() && objects_features[0].size() == 2)
	{
		for (unsigned int row = 0; row <objects_features.size(); ++row)
		{
			std::string objectId = objects_features[row][0];
			std::string trajId = objects_features[row][1];				
			ObjectIdtoPointList[objectId].push_back(trajectories[trajId]);
		}
	}
	updatePercent(70);
	std::map<unsigned int, DrawableSceneView*>& timestampToScene = getAppContext()->getTimestampToScene();
	int inc = 0;
	for(auto it = ObjectIdtoPointList.begin(); it != ObjectIdtoPointList.end(); ++it)
	{
		SceneObject* so = mObjectIdToObjectMap[it->first];
		QColor color(so->getColor().R, so->getColor().G, so->getColor().B);
		std::map<int, std::vector<cv::Point>> timestampToPointMap;
		const std::vector<TimestampToPoint>& pointList = it->second;
		for(auto ptListIt = pointList.begin(); ptListIt != pointList.end(); ++ptListIt)
		{
			const TimestampToPoint& pl = (*ptListIt);
			for(auto ptIt = pl.begin(); ptIt != pl.end(); ++ptIt)
				timestampToPointMap[(*ptIt).first].push_back((*ptIt).second);
		}

		for(auto timeIt = timestampToPointMap.begin(); timeIt != timestampToPointMap.end(); ++timeIt)
		{
			std::vector<cv::Point> ptList = (*timeIt).second;
	

			if(!ptList.empty())
			{
				cv::Rect bb = getBoundingBox(ptList);
				
				ObjectObservation* objObs = new ObjectObservation(color, false);
				objObs->setRect(QRectF(bb.x, bb.y, bb.width, bb.height));
				objObs->associateToSceneObject(so);
				int currentTimestamp = (*timeIt).first;
				auto it = timestampToScene.find(currentTimestamp);
				if(it == timestampToScene.end())
				{
					auto itPair = timestampToScene.insert(std::pair<unsigned int, DrawableSceneView*>(currentTimestamp, new DrawableSceneView(currentTimestamp, nullptr, getAppContext())));
					it = itPair.first;
				}
				DrawableSceneView* scene = (*it).second;
				scene->addItem(objObs);
			}
		}
		updatePercent(70+(float)inc/ObjectIdtoPointList.size());
		++inc;
	}
	return success;
}



void PolyTrackImporter::LoadDatabase()
{
	mObjectIdToObjectMap.clear();
	bool success = true;
	SQLiteManager sqlMgr(getLoadPath().toStdString());
	if(sqlMgr.isConnected())
	{
		updatePercent(10);
		bool objectTypeTableDefined = sqlMgr.tableExist("objects_type");
		bool objectsTableDefined = sqlMgr.tableExist("objects");
		bool boundingBoxTableDefined = sqlMgr.tableExist("bounding_boxes");
		bool objectsFeaturesTableDefined = sqlMgr.tableExist("objects_features");
		bool positionsTableDefined = sqlMgr.tableExist("positions");

		if((boundingBoxTableDefined || (positionsTableDefined && objectsFeaturesTableDefined)) && objectsTableDefined)
		{
			if(objectTypeTableDefined)
				success &= LoadObjectTypeTable(sqlMgr);
			
			success &= LoadObjectsTable(sqlMgr);
			updatePercent(30);
			if(boundingBoxTableDefined)			
				success &= LoadBoundingBoxesTable(sqlMgr);			
			else			
				success &= LoadPointsTable(sqlMgr);					
			
			updatePercent(100);
		}
		else		
			LOGERROR("Invalid table combination is unsupported.");
		
	}
	if(!success)
		LOGERROR("Error were generated during importation");
}


cv::Rect PolyTrackImporter::getBoundingBox(const std::vector<cv::Point>& pointList)
{
	unsigned int minX = -1, minY = -1;
	unsigned int maxX = 0, maxY = 0;

	for(auto ptIt = pointList.begin(); ptIt != pointList.end(); ++ptIt)
	{
		unsigned int x = (*ptIt).x;
		unsigned int y = (*ptIt).y;

		if(x > maxX)
			maxX = x;
		if(y > maxY)
			maxY = y;
		if(x < minX)
			minX = x;
		if(y < minY)
			minY = y;
	}

	int deltaX = maxX-minX;
	int deltaY = maxY-minY;
	

	return cv::Rect(minX, minY, deltaX > 0 ? deltaX: 1, deltaY > 0 ? deltaY: 1);
}