#include "UrbanTrackerExporter.h"

#include "StatusProgressBar.h"
#include "SQLiteManager.h"
#include "Logger.h"
#include "SceneObject.h"
#include <set>
#include <map>
#include "DrawableSceneView.h"
#include "AppContext.h"
#include "ObjectObservation.h"

UrbanTrackerExporter::UrbanTrackerExporter(StatusProgressBar* progressBarControl, AppContext* context)
: ExporterTask(progressBarControl, context, "UrbanTrack", "*.sqlite")
, mSQLManager(nullptr)
{

}

UrbanTrackerExporter::~UrbanTrackerExporter()
{

}

void UrbanTrackerExporter::run()
{
	mSQLManager = new SQLiteManager(mFilePath);
	if(mSQLManager->isConnected())
	{
		dropTables();
		createTables();
		saveCurrentContext();
	}
	else
	{
		//Log Error
	}
	
	delete mSQLManager;
}

bool UrbanTrackerExporter::createTables()
{
	bool success = mSQLManager->beginTransaction();
	success &= mSQLManager->executeStatement("create table object_type ( name TEXT, PRIMARY KEY( name) )");
	success &= mSQLManager->executeStatement("create table sceneobject ( object_id TEXT, type object_type, description TEXT, PRIMARY KEY( object_id) )");
	success &= mSQLManager->executeStatement("create table position ( object_id sceneobject, frame_number INTEGER, top_left_corner_x REAL, top_left_corner_y REAL, bottom_right_corner_x REAL, bottom_right_corner_y REAL,  PRIMARY KEY( object_id, frame_number ) )");
	success &= mSQLManager->endTransaction();
	return success;
}

bool UrbanTrackerExporter::dropTables()
{
	bool success = mSQLManager->beginTransaction();
	success &= mSQLManager->executeStatement("drop table if exists position;" );
	success &= mSQLManager->executeStatement("drop table if exists sceneobject;" );
	success &= mSQLManager->executeStatement("drop table if exists object_type;");
	success &= mSQLManager->endTransaction();
	return success; 
}





bool UrbanTrackerExporter::saveCurrentContext()
{
	//Sort and rename object
	cleanObjectsId();
	bool success = saveObjectTypeTable();
	success &= saveSceneObjectTable();
	this->updateProgressBarPercent(10);
	success &= savePositionTable();	
	return success;
}


bool UrbanTrackerExporter::saveSceneObjectTable()
{
	bool success = mSQLManager->beginTransaction();
	std::vector<SceneObject*>& sceneObjectList = mContext->getSceneObjectList();

	for(auto it = sceneObjectList.begin(); it != sceneObjectList.end();++it)
	{
		success &= mSQLManager->executeStatement("insert into sceneobject (object_id, type, description) values ('" + (*it)->getId() + "', '" +  (*it)->getType() + "', '" + (*it)->getDescription() + "');");
		if(!success)
		{
			LOGERROR("Error saving object " << (*it)->getId());
			success = true;
		}
	}

	success &= mSQLManager->endTransaction();
	return success;
}

bool UrbanTrackerExporter::saveObjectTypeTable()
{
	bool success = mSQLManager->beginTransaction();
	const ObjectTypeManager& manager = mContext->getObjectTypeManager();
	auto typeList = manager.getTypeList();
	std::stringstream ss;
	for(auto it = typeList.begin(); it != typeList.end();++it)
		ss << "insert into object_type (name) values ('" << (*it).second << "');";
	success &= mSQLManager->executeStatement(ss.str());
	success &= mSQLManager->endTransaction();
	return success;
}

bool UrbanTrackerExporter::savePositionTable()
{
	bool success = mSQLManager->beginTransaction();
	std::map<unsigned int, DrawableSceneView*>& timestampToScene= mContext->getTimestampToScene();
	int nbInc = 0;
	for (auto it = timestampToScene.begin(); it != timestampToScene.end();++it)
	{		
		updateProgressBarPercent(10+90.f*((float)nbInc/timestampToScene.size()));
		++nbInc;
		std::stringstream ss;
		unsigned int timestamp = (*it).first;
		DrawableSceneView* sceneView = (*it).second;
		QList<QGraphicsItem *> items = sceneView->items();
		for (int i = 0; i < items.size(); ++i)
		{
			ObjectObservation* obj = dynamic_cast<ObjectObservation*>(items[i]);
			if(obj)
			{
				QRectF rect = obj->rect();
				std::string objId = "unassociatedObject" + Utils::String::toString(i); //This id is reserved
				if(obj->getSceneObject())
					objId = obj->getSceneObject()->getId();

				int x1 = rect.topLeft().x() >= 0 ? rect.topLeft().x():0;
				x1 = x1 < mWidth ? x1 : mWidth-1;
				int y1 = rect.topLeft().y() >= 0 ? rect.topLeft().y():0;
				y1 = y1 < mHeight ? y1 : mHeight-1;
				int x2 = rect.bottomRight().x() < mWidth ? rect.bottomRight().x() : mWidth-1;
				x2 = x2 >=0 ? x2 : 0;
				int y2 = rect.bottomRight().y() < mHeight ? rect.bottomRight().y() : mHeight-1;
				y2 = y2 >=0 ? y2 : 0;
				int w = x2 - x1;
				int h = y2 - y1;
				if(w<0)
				{
					auto tmp = x2;
					x2 = x1;
					x1 = tmp;
				}
					
				if(h<0)
				{
					auto tmp = y2;
					y2 = y1;
					y1 = tmp;
				}
				
				if(h != 0 && w != 0)
				{
					ss << "insert into position (object_id, frame_number, top_left_corner_x, top_left_corner_y, bottom_right_corner_x, bottom_right_corner_y) values ('"
					<< objId << "', " << timestamp <<", " << x1 << ", " << y1 << ", "
					<< x2 << ", " << y2 << ");";
				}				
			}
		}	
		success &= mSQLManager->executeStatement(ss.str());
		if(!success)
		{
			LOGERROR("Error at " << timestamp);
			success = true;
		}
	}
	
	success &= mSQLManager->endTransaction();
	return success;
}