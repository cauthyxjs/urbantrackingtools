#include "ApplyMaskDlg.h"

#include "OpenCVHelpers.h"
#include <QString>

#include <map>
#include <QFileDialog>
#include "AppContext.h"
#include "DrawableSceneView.h"
#include "ObjectObservation.h"
#include "Logger.h"

ApplyMaskDlg::ApplyMaskDlg(QWidget *parent, AppContext* appContext)
: QDialog(parent) 
, mContext(appContext)
{
	mUi.setupUi(this);
	bool connected = connect(mUi.openFileBtnDlg, SIGNAL(clicked()), this, SLOT(ChooseFileDlg()));
}

void ApplyMaskDlg::getCorrectedPoint(const cv::Mat& mask, std::vector<cv::Point>& activePoint, std::vector<cv::Point>& maskedPoint)
{
	int MASKED = 0;
	int ACTIVE = 255;
	std::vector<bool> positiveIncX;
	std::vector<bool> positiveIncY;
	for(unsigned int m = 0; m < maskedPoint.size(); ++m)
	{
		for(unsigned int a = 0; a < activePoint.size(); ++a)
		{
			cv::Point deltaPt = maskedPoint[m]-activePoint[a];
			positiveIncX.push_back(deltaPt.x <  0);
			positiveIncY.push_back(deltaPt.y <  0);
		}

		std::vector<cv::Point> candidatePt;
		for(unsigned int i = 0; i < 3; ++i)
			candidatePt.push_back(maskedPoint[m]);

		bool found = false;
		while(!found)
		{
			for(unsigned int i = 0; i < positiveIncX.size(); ++i)
			{

				cv::Point& xCandidate = candidatePt[0];				
				cv::Point& yCandidate = candidatePt[1];				
				cv::Point& xyCandidate = candidatePt[2];
				xCandidate.x+=positiveIncX[i] ? 1 : -1;
				yCandidate.y+=positiveIncY[i] ? 1 : -1;
				xyCandidate.x+=positiveIncX[i] ? 1 : -1;
				xyCandidate.y+=positiveIncY[i] ? 1 : -1;
				
				if(xCandidate.x < mask.cols && xCandidate.y < mask.rows && xCandidate.x >=0 && xCandidate.y >= 0 && mask.at<unsigned char>(xCandidate.y, xCandidate.x) == ACTIVE)
				{
					activePoint.push_back(xCandidate);
					found = true;
				}
				else if(yCandidate.x < mask.cols && yCandidate.y < mask.rows && yCandidate.x >=0 && yCandidate.y >= 0 && mask.at<unsigned char>(yCandidate.y, yCandidate.x) == ACTIVE)
				{
					activePoint.push_back(yCandidate);
					found = true;
				}
				else if(xyCandidate.x < mask.cols && xyCandidate.y < mask.rows && xyCandidate.x >=0 && xyCandidate.y >= 0 && mask.at<unsigned char>(xyCandidate.y, xyCandidate.x) == ACTIVE)
				{
					activePoint.push_back(xyCandidate);
					found = true;
				}
			}
		}
	}
}


void ApplyMaskDlg::run()
{
	int MASKED = 0;
	int ACTIVE = 255;
	QString filePath = mUi.filePathTxtBox->text();
	cv::Mat mask = cv::imread(filePath.toStdString());
	cv::cvtColor(mask, mask, CV_BGR2GRAY);
	if(!mask.empty())
	{
		auto& timestampMap = mContext->getTimestampToScene();
		int nbBox = 0;
		for(auto itTimestamp = timestampMap.begin(); itTimestamp != timestampMap.end(); ++itTimestamp)
		{
			DrawableSceneView* sceneView =itTimestamp->second;
			if(sceneView)
			{
				QList<QGraphicsItem*> items = sceneView->items();
				for (auto itemIt = items.begin(); itemIt != items.end(); ++itemIt)					
				{
					bool deleted = false;
					ObjectObservation* obj = dynamic_cast<ObjectObservation*>(*itemIt);
					if(obj)
					{
						QRectF rect = obj->rect();
						cv::Point2f topLeftCorner(rect.x(), rect.y());
						cv::Point2f bottomRightCorner(rect.x() + rect.width(), rect.y() + rect.height());

						int minX = rect.x();
						int minY = rect.y();
						int maxX = rect.x() + rect.width();
						int maxY = rect.y() + rect.height();

						if(maxX >= mask.cols)
							maxX = mask.cols;
						if(maxY >= mask.rows)
							maxY = mask.rows;
						if(minX < 0)
							minX = 0;
						if(minY < 0)
							minY = 0;

						std::vector<cv::Point> corners;
						corners.push_back(cv::Point(minX, minY));
						corners.push_back(cv::Point(maxX, minY));
						corners.push_back(cv::Point(minX, maxY));
						corners.push_back(cv::Point(maxX, maxY));

						std::vector<cv::Point> maskedPoint;
						std::vector<cv::Point> activePoint;
						for(unsigned int ptIdx = 0; ptIdx < corners.size(); ++ptIdx)
						{
							int pxValue = mask.at<unsigned char>(corners[ptIdx].y, corners[ptIdx].x);
							if(pxValue == MASKED)
								maskedPoint.push_back(corners[ptIdx]);
							else if(pxValue == ACTIVE)
								activePoint.push_back(corners[ptIdx]);
							else
								LOGERROR("Unknown mask value " << pxValue);
								
						}

						if(maskedPoint.size() == 4 || (minX == maxX && minY == maxY))						
							deleted = true;						
						else
						{
							if(!maskedPoint.empty())
							{
								getCorrectedPoint(mask, activePoint, maskedPoint);
								int newMinX = maxX;
								int newMinY = maxY;
								int newMaxX = minX;
								int newMaxY = minY;
								for(unsigned int i = 0; i < activePoint.size(); ++i)
								{
									if(activePoint[i].x < newMinX)
										newMinX = activePoint[i].x;
									if(activePoint[i].y < newMinY)
										newMinY = activePoint[i].y;
									if(activePoint[i].x > newMaxX)
										newMaxX = activePoint[i].x;
									if(activePoint[i].y > newMaxY)
										newMaxY = activePoint[i].y;
								}

								obj->setRect(newMinX, newMinY, newMaxX - newMinX, newMaxY - newMinY);
							}

							++nbBox;
						}
						if(deleted)
						{
							sceneView->removeItem(*itemIt);
						}
						
							

					}
				}
			}
			
		}
		LOGINFO("Mask applied to " << nbBox << " bounding box");
	}
}


void ApplyMaskDlg::ChooseFileDlg()
{
	QString fileName = QFileDialog::getOpenFileName(this,
		tr("Load mask file"), "",
		tr("All Files (*)"));
	if(fileName != "")
	{
		mUi.filePathTxtBox->setText(fileName);
	}
}