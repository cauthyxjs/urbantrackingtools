#include "AppContext.h"

#include "SceneObject.h"
#include "DrawableSceneView.h"
#include "StringHelpers.h"

AppContext::AppContext(QtGroundTruthBuilderApp* gtBuilderApp)
: mVFM(nullptr)
, mId(-1)
, mGTBuilderApp(gtBuilderApp)
{
}


AppContext::~AppContext()
{
	clear();
}

SceneObject* AppContext::getNewObject()
{				
	SceneObject* so = new SceneObject(Utils::String::toString(++mId), "", "");
	mSceneObject.push_back(so);
	return so;
}

void AppContext::clear()
{
	for(auto it = mTimestampToScene.begin(); it != mTimestampToScene.end(); ++it)
		delete (*it).second;

	for(auto it = mSceneObject.begin(); it != mSceneObject.end(); ++it)
		delete (*it);


	mId = -1;
	mTimestampToScene.clear();
	mSceneObject.clear();
	mObjetManager.clear();

}

int AppContext::getObjectIdx(SceneObject* so) const
{
	int objectIdx = -1;
	for(unsigned int i = 0; i < mSceneObject.size() && objectIdx == -1; ++i)
	{
		if(mSceneObject[i] == so)
			objectIdx = i;
	}
	return objectIdx;
}