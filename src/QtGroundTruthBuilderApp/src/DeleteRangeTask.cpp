#include "DeleteRangeTask.h"
#include <vector>
#include "SceneObject.h"
#include <set>
#include "DrawableSceneView.h"
#include "ObjectObservation.h"
#include "AppContext.h"

 DeleteRangeTask::DeleteRangeTask(StatusProgressBar* progressBarControl, AppContext* appContext, std::map<QListWidgetItem*, SceneObject*> itemToObjectMap, std::set<SceneObject*> toDeleteSet, int startFrame, int endFrame, bool allTimestamp)
: BaseTask(progressBarControl, "Deleting range")
, mItemToObjectMap(itemToObjectMap)
, mContext(appContext)
, mToDeleteSet(toDeleteSet)
, mAllTimestamp(allTimestamp)
, mStartFrame(startFrame)
, mEndFrame(endFrame)
 {
 }

void DeleteRangeTask::run()
{
	std::map<SceneObject*, int> nbObservationByObject;
	for(auto it = mToDeleteSet.begin(); it != mToDeleteSet.end(); ++it)	
		nbObservationByObject[*it] = 0;
	
	auto& timestampMap = mContext->getTimestampToScene();
	int i = 0;
	for(auto it = timestampMap.begin(); it != timestampMap.end(); ++it)
	{
		this->updateProgressBarPercent(((float)i/(float)timestampMap.size())*100);
		++i;
		int timestamp = (*it).first;
		
		DrawableSceneView* dsv = (*it).second;
		std::vector<QGraphicsItem*> itemsToRemove;
		const QList<QGraphicsItem*>& items = dsv->items();
		for (auto itemIt = items.begin(); itemIt != items.end(); ++itemIt)
		{
			ObjectObservation* obj = dynamic_cast<ObjectObservation*>(*itemIt);
			if (obj)
			{
				bool deleted = false;
				if (mToDeleteSet.find(obj->getSceneObject()) != mToDeleteSet.end())
				{
					if (mAllTimestamp || (mStartFrame <= timestamp && timestamp <= mEndFrame))
					{
						itemsToRemove.push_back(*itemIt);
					}
					else
						++nbObservationByObject[obj->getSceneObject()];
				}
			}
			
		}

		for(unsigned int i = 0; i < itemsToRemove.size(); ++i)
			dsv->removeItem(itemsToRemove[i]);
	}
	std::set<SceneObject*> objectToRemove;
	for(auto it = nbObservationByObject.begin(); it != nbObservationByObject.end(); ++it)
	{
		if((*it).second == 0)			
			objectToRemove.insert((*it).first);			
	}

	std::vector<SceneObject*>& objectList = mContext->getSceneObjectList();
	for(auto it = objectList.begin(); it != objectList.end(); )
	{
		if(objectToRemove.find(*it) != objectToRemove.end())
			it = objectList.erase(it);
		else
			++it;
	}
}
