#include "TrafficIntelligenceGTImporter.h"
#include "StringHelpers.h"
#include <fstream>
#include "ObjectTypeManager.h"
#include <map>
#include "AppContext.h"
#include "SceneObject.h"
#include "ObjectObservation.h"
#include "DrawableSceneView.h"

TrafficIntelligenceGTImporter::TrafficIntelligenceGTImporter(StatusProgressBar* progressBarControl, AppContext* appContext, unsigned int videoWidth, unsigned int videoHeight)
: GTImporter(progressBarControl, appContext, "Traffic intelligence GT", "*.txt", "TRAFFICINT" ,GTImporter::FILE, videoWidth, videoHeight)
{

}


TrafficIntelligenceGTImporter::~TrafficIntelligenceGTImporter()
{

}



void TrafficIntelligenceGTImporter::LoadDatabase()
{
	ObjectTypeManager& manager = getAppContext()->getObjectTypeManager();
	std::map<unsigned int, std::string> idToNameMap;
	idToNameMap.insert(std::pair<unsigned int, std::string>(0, "cars"));
	idToNameMap.insert(std::pair<unsigned int, std::string>(1, "pedestrians"));
	idToNameMap.insert(std::pair<unsigned int, std::string>(2, "two-wheels"));
	idToNameMap.insert(std::pair<unsigned int, std::string>(3, "buses"));
	idToNameMap.insert(std::pair<unsigned int, std::string>(4, "trucks"));

	updatePercent(10);

	for(auto it = idToNameMap.begin(); it != idToNameMap.end(); ++it)
		manager.addType(it->second);
	

	std::ifstream t(getLoadPath().toStdString().c_str());
	std::string str;
	if (t.is_open())
	{
		t.seekg(0, std::ios::end);   
		str.reserve(t.tellg());
		t.seekg(0, std::ios::beg);
		str.assign((std::istreambuf_iterator<char>(t)),	std::istreambuf_iterator<char>());
		t.close();
	}
	std::vector<SceneObject*>& objList = getAppContext()->getSceneObjectList();
	unsigned int id = 0;
	StringArray lines = Utils::String::SplitInLines(str);
	for(unsigned int i = 0; i <lines.size(); ++i)
	{
		const std::string& currentLine = lines[i];
		if(!currentLine.empty() && currentLine[0] != '#') //Comments
		{
			if( i+4 < lines.size())
			{
				
				StringArray infos = Utils::String::Split(lines[i], " ");
				StringArray X = Utils::String::Split(lines[i+1], " ");
				StringArray Y = Utils::String::Split(lines[i+2], " ");
				StringArray W = Utils::String::Split(lines[i+3], " ");
				StringArray H = Utils::String::Split(lines[i+4], " ");
				
				if(X.size() == Y.size() && W.size() == H.size() && X.size() == W.size())
				{
				unsigned int startTimestamp, endtimestamp, objectType;
					bool castSuccess = Utils::String::StringToType(infos[1], startTimestamp);
					castSuccess &= Utils::String::StringToType(infos[2], endtimestamp);
					castSuccess &= Utils::String::StringToType(infos[3], objectType);
					SceneObject* so = new SceneObject(Utils::String::toString(id), "", idToNameMap[objectType]);
					objList.push_back(so);
					for(unsigned int obsNum = 0; obsNum < X.size(); ++obsNum)
					{
						auto it = getAppContext()->getTimestampToScene().find(startTimestamp+obsNum);
						if(it == getAppContext()->getTimestampToScene().end())
						{
							auto itPair = getAppContext()->getTimestampToScene().insert(std::pair<unsigned int, DrawableSceneView*>(startTimestamp+obsNum, new DrawableSceneView(startTimestamp+obsNum, nullptr, getAppContext())));
							it = itPair.first;
						}
						double Xv, Yv, Wv, Hv;
						castSuccess &= Utils::String::StringToType(X[obsNum], Xv);
						castSuccess &= Utils::String::StringToType(Y[obsNum], Yv);
						castSuccess &= Utils::String::StringToType(W[obsNum], Wv);
						castSuccess &= Utils::String::StringToType(H[obsNum], Hv);
						assert(castSuccess);
						DrawableSceneView* scene = it->second;
						ObjectObservation* objObs = new ObjectObservation(QColor(rand()%255,rand()%255, rand()%255), false);
						objObs->setRect(QRectF(Xv, Yv, Wv, Hv));
						objObs->associateToSceneObject(so);
						scene->addItem(objObs);
					}
					++id;
				}
				else
				{
					//LOG ERROR
				}
			}
			while(i < lines.size() && lines[i][0] != '%')
				++i;


		}
		updatePercent(10+ 90*(float)i/lines.size());
	}
	
}