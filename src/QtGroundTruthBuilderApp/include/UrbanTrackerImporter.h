#ifndef URBAN_TRACKER_IMPORTER_H
#define URBAN_TRACKER_IMPORTER_H

#include <QString>
#include "GTImporter.h"

class AppContext;


class UrbanTrackerImporter: public GTImporter
{
public:
	UrbanTrackerImporter(StatusProgressBar* progressBarControl,AppContext* appContext, unsigned int videoWidth, unsigned int videoHeight);
	virtual ~UrbanTrackerImporter();
	void LoadDatabase();
	virtual bool askHomography() { return false;}

};



#endif