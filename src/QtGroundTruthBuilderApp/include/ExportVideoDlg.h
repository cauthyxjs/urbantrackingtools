#ifndef EXPORT_VIDEO_DLG
#define EXPORT_VIDEO_DLG

#include "ui_ExportVideoDlg.h"

#include <qdialog.h>
#include "ProgressDialog.h"

class AppContext;
class InputFrameProviderIface;
class StatusProgressBar;
class BaseTask;


class ExportVideoDlg : public ProgressDialog
{
		Q_OBJECT
public:
	ExportVideoDlg(QWidget *parent, StatusProgressBar* progressBar, AppContext* appContext, InputFrameProviderIface* vfm);
	public slots:	
	virtual BaseTask* getBaseTask();
private slots:
	void ChooseFileDlg();
	
private:

	InputFrameProviderIface* m_np_Vfm;
	Ui::ExportVideoDlg mUi;
	AppContext* mContext;
};




#endif