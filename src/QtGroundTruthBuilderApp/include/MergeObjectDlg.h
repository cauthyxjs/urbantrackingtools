#ifndef MERGE_OBJECT_DLG
#define MERGE_OBJECT_DLG




#include <qdialog.h>
#include "ui_MergeSelectionListDlg.h"
#include <vector>
class AppContext;
class SceneObject;

class MergeObjectDlg: public QDialog
{
		Q_OBJECT
public:
		MergeObjectDlg(QWidget *parent, AppContext* appContext);
		void run();


private:
	Ui::MergeSelectionListDlg mUi;
	AppContext* mContext;
	std::map<QListWidgetItem*, SceneObject*> mItemToObjectMap;
};


#endif