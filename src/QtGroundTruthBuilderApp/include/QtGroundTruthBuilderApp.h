#ifndef QTGROUNDTRUTHBUILDERAPP_H
#define QTGROUNDTRUTHBUILDERAPP_H

#include <QMainWindow>
#include "ui_QtGroundTruthBuilderApp.h"
#include "ObjectTypeManager.h"
#include <QKeyEvent>
#include <opencv2/opencv.hpp>
class Scene;
class InputVideoFileModule;
class DrawableSceneView;

class BlobDetector;
class AppContext;
class GTImporter;
class NegativeSampleDlg;
class QTimer;
class InputFrameProviderIface;
class SceneObject;
class QProgressBar;
class QLabel;
class StatusProgressBar;
class BaseTask;
class ExporterTask;

class ProgressDialog;

class QtGroundTruthBuilderApp : public QMainWindow
{
	Q_OBJECT
	enum ObjectTypeUi
	{
		EXISTINGTYPE,
		NEWTYPE
	};
public:
	QtGroundTruthBuilderApp(QWidget *parent = 0, Qt::WindowFlags flags = 0);
	~QtGroundTruthBuilderApp();
	void resizeEvent(QResizeEvent* event);
	virtual void keyPressEvent(QKeyEvent *event);
	SceneObject* getFrameListCurrentObject();
public slots:

	void LoadVideo(QString path);
	void LoadVideo();
	void LoadDatabase();
	void LoadDatabase(const QString& type, const QString& path);
	void LoadDatabase(GTImporter* importer, const QString& path);
	void OpenNegativeSampleDlg();
	void OnInterpolateBtnTriggered();
	void SaveDatabase();
	void SaveCurrentFrame();
	void SaveCurrentFrameAnnotation();
	void DisplayMaxNbBoxes();
	void OpenSaveAnnotatedVideoDlg();
	void GoToNextFrame();
	void GoToPreviousFrame();
	void SceneObjectSelectionChanged();
	void DescriptionFinish();
	void UpdateCurrentFrameObjectList();
	void OnNewObjectAdded();
	void onListItemSelectionChange();
	void UpdateObjectIdList();
	void ExtractSubImage();
	void ExtractNegSubImage(int minX, int maxX, int minY, int maxY, int nbSamples);
	void countNumberOfBox();
	void displayAboutDlg();
	void onPlayPause();
	void onPlayPauseNewFrame();
	void seekToFrame(int fNumber);
	void OpenMergeObjectDlg();
	void OpenChangeIdDlg();
	void OpenDeleteRangeDlg();
	void OpenHomographyDlg();
	void OpenMaskDlg();
	void OnObjectTypeSelection(int);
	void OnObjectIdSelection(int);
	void UpdateObjectTypeList();
	QString AddNewObjectType();
	AppContext* getContext() {return mAppContext;}
	void runAsyncImport(GTImporter*);
	void finishImport();
	void finishTask();
	const std::map<QString, GTImporter*>& getImporterList() const { return mNameToImporter;}
	InputFrameProviderIface* getVideoInput() { return mVfm;}
signals:
	void OnFrameChange();
	
private:
	void registerExporters();
	void registerImporters();
	void runAsyncTask(ProgressDialog* dlg);
	void runAsyncTask(BaseTask* task);
	void updateGraphicsViewScale();
	Ui::QtGroundTruthBuilderAppClass mUi;
	DrawableSceneView* mCurrentGraphicsScene;
	InputFrameProviderIface* mVfm;
	bool mInitOnFirstFrame;


	std::map<QString, ExporterTask*> mNameToExporter;
	std::map<QAction*, ExporterTask*> mActionToExporter;

	std::map<QString, GTImporter*> mNameToImporter;
	std::map<QAction*, GTImporter*> mActionToImporter;
	AppContext* mAppContext;
	StatusProgressBar* mProgressBar;
	QTimer* mPlayTimer;
	BaseTask* mCurrentTask;
	QThread* mCurrentTaskThread;
	cv::Mat mCurrentFrameMat;
	float mCurrentGraphicsViewScaleFactor;
};

#endif // QTGROUNDTRUTHBUILDERAPP_H
