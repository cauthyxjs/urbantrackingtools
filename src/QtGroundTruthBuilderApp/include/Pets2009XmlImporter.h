#ifndef PETS2009_XML_IMPORTER_H
#define PETS2009_XML_IMPORTER_H

#include <QString>
#include <map>
#include "GTImporter.h"

class AppContext;

class Pets2009XmlImporter: public GTImporter
{
public:
	Pets2009XmlImporter(StatusProgressBar* progressBarControl, AppContext* appContext,unsigned int videoWidth, unsigned int videoHeight);	
	virtual ~Pets2009XmlImporter();

	bool askHomography() { return false;}
	virtual void LoadDatabase();

};

#endif