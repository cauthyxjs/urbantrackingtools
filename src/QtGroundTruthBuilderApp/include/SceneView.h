#ifndef SCENE_VIEW_H
#define SCENE_VIEW_H

#include <QGraphicsView>

class SceneView : public QGraphicsView
{
	Q_OBJECT
public:
	SceneView(QWidget* p) : QGraphicsView(p){}
	virtual ~SceneView() {}
	virtual void mouseDoubleClickEvent (QMouseEvent * event )
	{
		emit onDoubleClick(event);
	}
signals: 
	void onDoubleClick(QMouseEvent * event);

};



#endif