#ifndef BASE_TASK_H
#define BASE_TASK_H

class StatusProgressBar;
#include "qstring.h"
#include "qobject.h"
#include "qtimer.h"
class BaseTask : public QObject 
{
Q_OBJECT
public:
	BaseTask(StatusProgressBar* progressBarControl, QString taskDescription);
	virtual ~BaseTask();

	void updateProgressBarPercent(int percent);
	void setProgressBarText(QString text);
	void enableProgressBar(bool enable);
	
	virtual bool isOwnedByThread() = 0;
public slots:
		void process();
signals:
		void finished();
		void error(QString err);
		void updatePercent(int);
		void updateText(QString);
		void displayUi(bool);

private:
	virtual void run() = 0;
	
private:

	StatusProgressBar* mProgressBarController;
	QString mTaskDescription;


};

#endif