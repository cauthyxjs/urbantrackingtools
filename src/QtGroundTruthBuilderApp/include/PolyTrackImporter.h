#ifndef POLYTRACKIMPORTER_H
#define POLYTRACKIMPORTER_H


#include <QString>
#include "GTImporter.h"
#include <vector>
#include <opencv2/opencv.hpp>
#include "SQLiteManager.h"
class AppContext;
class SceneObject;
class PolyTrackImporter: public GTImporter
{
public:
	PolyTrackImporter(StatusProgressBar* progressBarControl, AppContext* appContext, unsigned int videoWidth, unsigned int videoHeight);
	virtual ~PolyTrackImporter();
	bool askHomography() {return true;}
	void LoadDatabase();
private:
	bool LoadObjectTypeTable(SQLiteManager& sqlMgr);
	bool LoadObjectsTable(SQLiteManager& sqlMgr);
	bool LoadBoundingBoxesTable(SQLiteManager& sqlMgr);
	bool LoadPointsTable(SQLiteManager& sqlMgr);


	std::map<std::string, SceneObject*> mObjectIdToObjectMap;
	cv::Rect getBoundingBox(const std::vector<cv::Point>& points);
};


#endif