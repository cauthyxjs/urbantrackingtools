#ifndef STATUS_PROGRESS_BAR_H
#define STATUS_PROGRESS_BAR_H

#include <qobject.h>
#include <qstring.h>

class QProgressBar;
class QLabel;
class QStatusBar;


class StatusProgressBar : public QObject
{

	Q_OBJECT
public:
	StatusProgressBar(QStatusBar* statusBar);
public slots:
	void setProgressPercent(int progress);
	void setProgressText(QString text);
	void displayProgressControls(bool enable);

private:
	QProgressBar* mProgressBar;
	QLabel* mProgressText;

};

#endif