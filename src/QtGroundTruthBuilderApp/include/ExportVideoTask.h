#ifndef EXPORT_VIDEO_TASK_H
#define EXPORT_VIDEO_TASK_H

#include "BaseTask.h"
#include <qstring.h>

class InputFrameProviderIface;
class AppContext;

class ExportVideoTask : public BaseTask
{
	 Q_OBJECT
 
public:
    ExportVideoTask(StatusProgressBar* progressBarControl, QString filePath, bool displayTrail, bool displayBoundingBox, int startTime, int endTime, float fps, InputFrameProviderIface* vfm, AppContext* appContext);
	~ExportVideoTask() {}
	bool isOwnedByThread() {return true;}

private:
	virtual void run();
	QString mFilePath;
	bool mDisplayTrail;
	bool mDisplayBoundingBox;
	int mStartFrame;
	int mEndFrame;
	float mFPS;
	InputFrameProviderIface* m_np_Vfm;
	AppContext* mContext;
};

#endif