#ifndef APPLY_MASK_DLG_H
#define APPLY_MASK_DLG_H

#include <qdialog.h>
#include "ui_ApplyMaskDlg.h"
#include <map>
#include <vector>
#include <opencv2/opencv.hpp>

class AppContext;
class SceneObject;

class ApplyMaskDlg: public QDialog
{
		Q_OBJECT
public:
		ApplyMaskDlg(QWidget *parent, AppContext* appContext);
		void run();
		void getCorrectedPoint(const cv::Mat& mask, std::vector<cv::Point>& activePoint, std::vector<cv::Point>& maskedPoint);

private slots:
		void ChooseFileDlg();

private:
	Ui::ApplyMaskDlg mUi;
	AppContext* mContext;

};
#endif