#include "GTSystemDataAssociator.h"
#include "Observation.h"
#include "MetricsHelpers.h"
#include <iostream>
#include <assert.h>


GTSystemDataAssociator::GTSystemDataAssociator ( std::vector<SceneObjectWithObservation*>* gtObjects, std::vector<SceneObjectWithObservation*>* systemObjects, double minTemporalOverlap, double minSpatialOverlap )
{
	BuildAssociations(gtObjects, systemObjects, minTemporalOverlap, minSpatialOverlap);
}

GTSystemDataAssociator::~GTSystemDataAssociator()
{

}

void GTSystemDataAssociator::AddAssociation(Association& a)
{
	//Association to ground truth
	auto gtIT = mGTAssociation.find(a.getGroundTruth());
	if(gtIT == mGTAssociation.end()) 
	{
		gtIT = mGTAssociation.insert(std::pair<SceneObjectWithObservation*, std::list<Association>>(a.getGroundTruth(), std::list<Association>())).first;
	}
	
	gtIT->second.push_back(a);
	//Association to system
	auto systemIt = mSystemAssociation.find(a.getSystem());
	if(systemIt == mSystemAssociation.end())
	{
		systemIt = mSystemAssociation.insert(std::pair<SceneObjectWithObservation*, std::list<Association>>(a.getSystem(), std::list<Association>())).first;
	}
	systemIt->second.push_back(a);

}

void GTSystemDataAssociator::RemoveAssociation(const Association& a)
{
	auto gtIT = mGTAssociation.find(a.getGroundTruth());
	if(gtIT != mGTAssociation.end())
	{
		auto gtList = gtIT->second;
		for (auto it = gtList.begin(); it != gtList.end(); )
		{
			if(it->getSystem() == a.getSystem())
				it = gtList.erase(it); // This will autoincrement the iterator
			else
				++it;			
		}
	}

	auto sysIT = mSystemAssociation.find(a.getSystem());
	if(sysIT != mSystemAssociation.end())
	{
		auto systemList = sysIT->second;
		for (auto it = systemList.begin(); it != systemList.end(); )
		{
			if(it->getGroundTruth() == a.getGroundTruth())
				it = systemList.erase(it); // This will autoincrement the iterator
			else
				++it;			
		}
	}
}


void GTSystemDataAssociator::RemoveAllAssociationFromList(const std::vector<Association>& associationList)
{
	for (auto it = associationList.begin(); it != associationList.end(); ++it)
	{
		RemoveAssociation((*it));
	}
}


void GTSystemDataAssociator::BuildAssociations( std::vector<SceneObjectWithObservation*>* gtObjectList, std::vector<SceneObjectWithObservation*>* systemObjectList, double minTemporalOverlap, double minSpatialOverlap  )
{
	
	//We create the first association using the paper parameters
	for(unsigned int i = 0; i < gtObjectList->size(); ++i)
	{
		SceneObjectWithObservation* gtObj = (*gtObjectList)[i];
		const std::vector<Observation*>& gtObservations = gtObj->mObservations;
		if(!gtObservations.empty())
		{		
			for(unsigned int j = 0; j < systemObjectList->size(); ++j)
			{
				SceneObjectWithObservation* systemObj = (*systemObjectList)[j];
				const std::vector<Observation*>& systemObservations = systemObj->mObservations;
				int startOverlap, endOverlap, temporalIntersection, temporalUnion;
				if(MetricsHelpers::temporalOverlap(gtObservations, systemObservations, startOverlap, endOverlap, temporalIntersection, temporalUnion))
				{
					double temporalOverlap = ((double)temporalIntersection) / ((double)gtObservations.size());
					if(temporalOverlap >= minTemporalOverlap)//Temporal overlap criterion, Condition 1
					{
						double accumulator = 0;
						//Condition 2, Spatial overlap
						for(int currentTimestamp = startOverlap; currentTimestamp <= endOverlap; ++currentTimestamp)
						{
							Observation* gtObs = MetricsHelpers::getObservation(gtObservations, currentTimestamp);
							Observation* sysObs = MetricsHelpers::getObservation(systemObservations, currentTimestamp);
							if(gtObs && sysObs)							
								accumulator += MetricsHelpers::spatialOverlap(gtObs, sysObs);							
						}
						double spatOverlap = accumulator/temporalIntersection;
						if(spatOverlap >= minSpatialOverlap)						
						{
							Association a(gtObj, systemObj, spatOverlap, temporalOverlap);								
							AddAssociation(a);
						}
					}
				}
			}
		}
		else
		{
			std::cout << "Error: No ground truth observation for object" << std::endl;
		}
	}
	
	// We don't want system track to be linked to more than one GT object. We will use some criteria to break association and try to resolve ambiguity correctly.
	// This loop is separated to allow more specific complex criterion
	for(auto it = mSystemAssociation.begin(); it != mSystemAssociation.end(); ++it)
	{
		auto currentAssociations = it->second;
		
		if(currentAssociations.size() > 1)
		{
			//We need to break association
			double spatialOverlapByTime = 0;
			std::vector<Association> irrelevantLink;
			Association* bestAssociation = nullptr;
			for(auto associationIt = currentAssociations.begin(); associationIt != currentAssociations.end(); ++associationIt)
			{
				//Maybe we could combine this with temporal overlap to make sure we have the best criterion, handle id change for system track ?
				if(associationIt->getSpatialOverlap() > spatialOverlapByTime)
				{
					if(bestAssociation)
						irrelevantLink.push_back(*bestAssociation);
					spatialOverlapByTime = associationIt->getSpatialOverlap();
					bestAssociation = &(*associationIt);
				}
				else
					irrelevantLink.push_back(*associationIt);
			}
			RemoveAllAssociationFromList(irrelevantLink);
		
				

			assert(currentAssociations.size() == 1);
		}
	}

	//Write data to finale structure
	for(auto it = (*systemObjectList).begin(); it != (*systemObjectList).end(); ++it)
	{
		SceneObjectWithObservation* associatedGT = nullptr;
		auto systemObjectAssociation = mSystemAssociation.find(*it);
		if(systemObjectAssociation != mSystemAssociation.end())
		{
			auto association = systemObjectAssociation->second;
			if(!association.empty())
				associatedGT = (association.begin())->getGroundTruth();
		}	
		mSystemToGroundTruth.insert(std::pair<SceneObjectWithObservation*, SceneObjectWithObservation*>((*it), associatedGT));
	}


	for(auto it = (*gtObjectList).begin(); it != (*gtObjectList).end(); ++it)
	{
		std::list<SceneObjectWithObservation*> listSystemObject; 

		auto gtbjectAssociation = mGTAssociation.find(*it);
		if(gtbjectAssociation != mGTAssociation.end())
		{
			auto associationList = gtbjectAssociation->second;
			for(auto itAssoc = associationList.begin(); itAssoc != associationList.end(); ++itAssoc)
			{
				listSystemObject.push_back((*itAssoc).getSystem());
			}			
		}	
		mGtToSystem.insert(std::pair<SceneObjectWithObservation*, std::list<SceneObjectWithObservation*>>((*it), listSystemObject));
	}
}
