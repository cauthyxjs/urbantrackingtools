#ifndef GT_SYSTEM_DATA_ASSOCIATOR_H
#define GT_SYSTEM_DATA_ASSOCIATOR_H

#include <list>
#include <vector>
#include <map>
#include "Association.h"

struct SceneObjectWithObservation;

class GTSystemDataAssociator
{
public:
	GTSystemDataAssociator(std::vector<SceneObjectWithObservation*>* gtObjects, std::vector<SceneObjectWithObservation*>* systemObjects, double minTemporalOverlap, double minSpatialOverlap );
	~GTSystemDataAssociator();
	void AddAssociation(Association& a);
	void RemoveAssociation(const Association& a);
	void RemoveAllAssociationFromList(const std::vector<Association>& associationList);

	std::map<SceneObjectWithObservation*, SceneObjectWithObservation*>& getSystemToGTMap() { return mSystemToGroundTruth;}
	std::map<SceneObjectWithObservation*, std::list<SceneObjectWithObservation*>>& getGTToSystemMap() { return mGtToSystem;}

private:
	void BuildAssociations(std::vector<SceneObjectWithObservation*>* gtObjects, std::vector<SceneObjectWithObservation*>* systemObjects, double minTemporalOverlap, double minSpatialOverlap );


	std::map<SceneObjectWithObservation*, SceneObjectWithObservation*> mSystemToGroundTruth;
	std::map<SceneObjectWithObservation*, std::list<SceneObjectWithObservation*> > mGtToSystem;

	std::map<SceneObjectWithObservation*, std::list<Association> > mGTAssociation;
	std::map<SceneObjectWithObservation*, std::list<Association> > mSystemAssociation;

};


#endif
