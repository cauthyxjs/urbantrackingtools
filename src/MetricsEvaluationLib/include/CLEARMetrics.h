#ifndef CLEAR_METRICS_H
#define CLEAR_METRICS_H

#include <string>
#include <iostream>
#include <sstream>
#include "Metrics.h"
#include "MeasureDistType.h"

class CLEARMetrics: public Metrics
{
public:
	CLEARMetrics()
	: mMOTP(-1)
	, mMOTA(-1)
	, mMissesRatio(-1)
	, mFPRatio(-1)
	, mMismatchesRatio(-1)
	{}


	CLEARMetrics(double motp, double mota, double missesRatio, double fpRatio, double mismatchesRatio, MeasureDistType distType)
	: mMOTP(motp)
	, mMOTA(mota)
	, mMissesRatio(missesRatio)
	, mFPRatio(fpRatio)
	, mMismatchesRatio(mismatchesRatio)
	, mDistType(distType)
	{
	}
	~CLEARMetrics()
	{
	}

	virtual void displayResults()
	{
		std::cout   << "=========================== CLEAR Metrics ============================" << std::endl;	
		std::cout	<< "MOTA                                              " << mMOTA << std::endl;
		if(mDistType == ABSOLUTEDIST)
			std::cout	<< "MOTP (absolute)                                   " << mMOTP << "px"<<std::endl;
		else if(mDistType == BOUNDINGBOXOVERLAP)
			std::cout	<< "MOTP (overlap)                                    " << mMOTP <<std::endl;
		else
			std::cout << "MOTP (Unknown distance type)                        " << mMOTP <<std::endl;
		std::cout	<< "Misses Ratio                                      " << mMissesRatio << std::endl;
		std::cout	<< "False Positive Ratio	                          " << mFPRatio << std::endl;
		std::cout	<< "Mismatches Ratio (id change)                      " << mMismatchesRatio << std::endl;
		std::cout	<< "======================================================================" << std::endl;
	}
	virtual std::string serialize()
	{
		std::stringstream ss;
		ss << "MOTA;" << mMOTA << "\n";
		ss << "MOTP;" << mMOTP << "\n";
		ss << "MISSES_RATIO;" << mMissesRatio << "\n";
		ss << "FP_RATIO;" << mFPRatio << "\n";
		ss << "MME_RATIO;" << mMismatchesRatio << "\n";
		return ss.str();
	}


	double mMOTP;
	double mMOTA;
	double mMissesRatio;
	double mFPRatio;
	double mMismatchesRatio;
	MeasureDistType mDistType;
};

#endif