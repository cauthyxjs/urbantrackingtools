/*************************************************************************
*	C++ implementation of Performance Evaluation of Object Tracking 
*	Algorithms (Fei Yin, Dimitrios Makris and Sergio Velastin 2007)
*
*  Implemented by Jean-Philippe Jodoin
*************************************************************************/
#ifndef METRICS_CALCULATOR_H
#define METRICS_CALCULATOR_H

#include <string>

#include "GTSystemDataAssociator.h"
#include <sstream>
#include <iostream>
#include "PerfEvalOTAMetrics.h"

struct SceneObjectWithObservation;
class Dataset;




class PerfEvalOTAMetricsCalculator
{
public:
	PerfEvalOTAMetricsCalculator(const Dataset* GTPath, const Dataset* experimentalPath, double temporalOverlap = 0.15, double spatialOverlap = 0.20);
	~PerfEvalOTAMetricsCalculator();

	unsigned int getNbGTTracks();
	unsigned int getNbSystemTracks();
	unsigned int getCDT();
	unsigned int getFAT();
	unsigned int getTDF();
	unsigned int getTF();
	unsigned int getIDC();
	double getLT();
	double getCTM();
	double getCTD();
	double getTMEMT();
	double getTMEMTD();
	double getTCM();
	double getTCD();
	PerfEvalOTAMetrics calculateMetrics();
	

	//static bool spatialOverlap(const Rect& A, const Rect& B);
	
	//Helpers function

	//static int getStartIndex(
private:
	//The name is not so good. We made this function because getFat is the inverse of getTDF;
	unsigned int getWrongDetectionScore(const std::vector<SceneObjectWithObservation*>& detectionList, const std::vector<SceneObjectWithObservation*>& toBeDetected);
	GTSystemDataAssociator* mDataAssociator;
	
	
	const Dataset* mGroundTruth; 
	const Dataset* mSystem;


	//Metrics threshold and parameters
	double mT_ov; //minimum spatial overlap between 2 objects to be considered the same
	double mTR_ov;//minimum temporal overlap for 2 objects to be considered the same


};


#endif 