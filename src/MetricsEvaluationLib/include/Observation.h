#ifndef OBSERVATION_H
#define OBSERVATION_H

#include "Rect.h"
#include "SceneObject.h"

//class SceneObject;

struct SceneObjectWithObservation;

struct Observation
{
	Observation(unsigned int timestamp, Rect rectangle, SceneObjectWithObservation* obj)
	: mTimestamp(timestamp)
	, mRectangle(rectangle)
	, mObject(obj)
	, mAssociated(false)
	{

	}
	unsigned int mTimestamp;
	Rect mRectangle;
	SceneObjectWithObservation* mObject;
	bool mAssociated;
};


struct SceneObjectWithObservation
{
	SceneObject* mSceneObject;
	std::vector<Observation*> mObservations;

	float getTrackRatio()
	{
		int mTracked = 0;
		for (Observation* obs : mObservations)
		{
			if (obs->mAssociated)
				++mTracked;
		}
		return (float)mTracked / (float) mObservations.size();
	}


	bool isMostlyTracked()
	{
		return getTrackRatio() >= 0.8;
	}
	bool isMostlyLost()
	{
		return getTrackRatio() <= 0.2;
	}

};




#endif